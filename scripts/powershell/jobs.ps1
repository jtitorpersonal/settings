<#
.SYNOPSIS
Job management methods
#>

function start-in-background() {
	<#
	.SYNOPSIS
	Runs the given script block in the background.

	.DESCRIPTION
	If ShowNotifications is set, it'll also show messages when the job completes
	or fails.

	.PARAMETER Name
	The name of the job. Default is "Background Job".

	.PARAMETER ScriptBlock
	The actual code to run. Default is an empty block.

	.PARAMETER ShowNotifications
	If true, job will display a notification method on success or failure.
	Default is $true.

	#>
	param(
		[string]$Name = "Background Job",
		[string]$ScriptBlock = { },
		[bool]$ShowNotifications = $true
	)

	$actualScriptBlock = [scriptblock]::Create($ScriptBlock)

	#Generate an internal job name.
	$jobName = -join ((48..57) + (97..122) | Get-Random -Count 6 | % {[char]$_})

	$finalBlock = {
		. .\notifications.ps1
		try {
			Invoke-Command -ScriptBlock $actualScriptBlock
			Write-Host "Job complete?"
			notify-toast -Title "Job Complete: $Name" -Content "Job complete!"
		}
		catch {
			$failureReason = $error[0].ToString() + $error[0].InvocationInfo.PositionMessage
			Write-Host $failureReason
			notify-toast -Title "Job Failed: $Name" -Content "Get details with 'get-job -Name `"$jobName`"'.`n`nFailure reason: $failureReason" -Lifetime 30
		}
	}

	#notify-toast -Title "Starting Job: $Name" -Content "Starting job..."
	$job = start-job -ScriptBlock $finalBlock -Name $jobName
	$job | Wait-Job
}