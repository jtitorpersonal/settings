# Summary
Common Powershell functions and scripts.

# Importing a Script
To use functions from the CLI directly, you need to do an import:

`. .\scriptnamehere.ps1`

This will import `scriptnamehere.ps1`'s fields for use in the CLI.