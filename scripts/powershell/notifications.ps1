<#
.SYNOPSIS
Methods for making notifications.
#>

function notify-toast() {
	<#
	.SYNOPSIS
	Displays a toast notification; Windows 10 and up only.

	.DESCRIPTION
	This creates one of those little notifications that pops up to the right of the screen.
	It behaves just like a normal notification, so it can lso be made hidden so you only see it when manually viewing
	the Notification Center too.

	.PARAMETER Title
	The title of the notification. Default is "Default Title".

	.PARAMETER Content
	The body of the notification. Default is "Default Content".

	.PARAMETER Lifetime
	The lifetime of the notification in seconds. Default is 5.

	.PARAMETER SuppressPopup
	If true, notification will not pop up. Default is $false.

	.EXAMPLE 
	Make a normal notification that reads "Hi!" and dismisses after 10 seconds:
	notify-toast -Title "Hello Powershell" -Content "Hi!" -Lifetime 10

	.NOTES
	I don't know what happens if this is run under anything earlier than Windows 8,
	but it probably won't work.
	#>
	param(
		#Notification title.
		[string]$Title = "Default Title",
		#Notification content.
		[string]$Content = "Default Content",
		#Notification lifetime in seconds.
		[long]$Lifetime = 5,
		#If true, notification will not pop up.
		[bool]$SuppressPopup = $false
	)

	#`> $null` means "silence standard output".
	[Windows.UI.Notifications.ToastNotificationManager, Windows.UI.Notifications, ContentType = WindowsRuntime] > $null
	#Get a toast template that's just a text label.
	$template = [Windows.UI.Notifications.ToastNotificationManager]::GetTemplateContent([Windows.UI.Notifications.ToastTemplateType]::ToastText02)

	#Convert to .NET type for XML manipuration
	$toastXml = [xml] $template.GetXml()
	$textXml = $toastXml.GetElementsByTagName("text")
	$textXml[0].AppendChild($toastXml.CreateTextNode($Title)) > $null
	$textXml[1].AppendChild($toastXml.CreateTextNode($Content)) > $null

	#Convert back to WinRT type
	$xml = New-Object Windows.Data.Xml.Dom.XmlDocument
	$xml.LoadXml($toastXml.OuterXml)

	$toast = [Windows.UI.Notifications.ToastNotification]::new($xml)
	$toast.Tag = "PowerShell"
	$toast.Group = "PowerShell"
	$toast.ExpirationTime = [DateTimeOffset]::Now.AddSeconds($Lifetime)
	$toast.SuppressPopup = $SuppressPopup

	$notifier = [Windows.UI.Notifications.ToastNotificationManager]::CreateToastNotifier("PowerShell")
	$notifier.Show($toast);
}

function notify-taskbar() {
	<#
	.SYNOPSIS
	Displays a taskbar bubble notification.
	Should be good for anything from XP on.

	.DESCRIPTION
	This creates a pop-up bubble notification in the taskbar.

	.PARAMETER Title
	The title of the notification. Default is "Default Title".

	.PARAMETER Content
	The body of the notification. Default is "Default Content".

	.PARAMETER Lifetime
	The lifetime of the notification in seconds. Default is 5.

	.EXAMPLE 
	Make a normal notification that reads "Hi!" and dismisses after 7 seconds:
	notify-taskbar -Title "Hello Taskbar" -Content "Hi!" -Lifetime 7
	#>
	param(
		$Title = "Default Title",
		$Content = "Default Content",
		$Lifetime = 5
	)

	[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")

	$objNotifyIcon = New-Object System.Windows.Forms.NotifyIcon

	#$objNotifyIcon.Icon = "C:\Scripts\Forms\Folder.ico"
	#$objNotifyIcon.BalloonTipIcon = "Error"
	$objNotifyIcon.BalloonTipText = $Content
	$objNotifyIcon.BalloonTipTitle = $Title
	
	$objNotifyIcon.Visible = $True
	#Convert lifetime to milliseconds.
	$objNotifyIcon.ShowBalloonTip($Lifetime * 1000)
}