class ShowHelp:
    @classmethod
    def show_help(cls):
        print("Loads a save from the specified directory/.zip file, then replaces your current save with it.")
        print("The app automatically finds your save dir depending on your platform!")
        print("As long, uh, as it's not Linux :(")