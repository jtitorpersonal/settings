from ...save_list_verifier import SaveListVerifier
from ...utilities import Utilities

_ask_message = "Load files anyway?"

class SanityChecker:
    @classmethod
    def check(cls, source_file_list, dest_path, backup_path):
        SaveListVerifier.verify_save_files(source_file_list, _ask_message)
        cls._verify_dest_path(dest_path)
        cls._verify_backup_path(backup_path, source_file_list)

    @classmethod
    def _verify_dest_path(cls, dest_path):
        Utilities.verify_path_exists(dest_path)
        Utilities.verify_can_read_path(dest_path)

    @classmethod
    def _verify_backup_path(cls, backup_path, source_file_list):
        try:
            cls._verify_files_do_not_already_exist(backup_path, source_file_list)
        except DuplicateFilesError as e:
            cls._notify_duplicate_files(e)
            Utilities.ask_if_should_continue(e, _ask_message)
        
    @classmethod
    def _verify_files_do_not_already_exist(cls, backup_path, source_file_list):
        files_at_backup = cls._get_files_at_path(backup_path)
        duplicate_files = cls._compare_file_lists(files_at_backup, source_file_list).duplicates

        if duplicate_files:
            raise NotImplementedError()
            #raise DuplicateFilesError(duplicate_files)
        
    @classmethod
    def _notify_duplicate_files(cls, caught_error):
        raise NotImplementedError()