from ..utilities import Utilities, clean_multiline
from .impl.sanity_checker import SanityChecker

class Loader:
    @classmethod
    def run(cls, source_path, backup_path, dry_run):
        dest_path = Utilities.get_save_folder()
        source_path_files = Utilities.get_save_list(source_path)

        SanityChecker.check(source_path_files, dest_path, backup_path)
        cls._ask_user_confirmation()
        cls._perform(source_path_files, dest_path, backup_path, dry_run)

    @classmethod
    def _ask_user_confirmation(cls, source_path, dest_path, backup_path):
        print(
            clean_multiline(
                f"""
                Loading all save data from '{source_path}' to '{dest_path}'.
                (Any existing data in the destination will be backed up to '{backup_path}'.)
                Confirm?
                """
            )
        )
        raise NotImplementedError()

    @classmethod
    def _perform(cls, source_path_files, dest_path, backup_path, dry_run):
        #Backup *first*, then do load
        cls._do_backup(dest_path, backup_path, dry_run)
        Utilities.copy_files(source_path_files, dest_path, dry_run)

    @classmethod
    def _do_backup(cls, dest_path, backup_path, dry_run):
        dest_path_files = Utilities.get_save_list(dest_path)

        if dest_path_files:
            Utilities.copy_files(dest_path_files, backup_path, dry_run)