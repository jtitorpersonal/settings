from ..utilities import Utilities, clean_multiline
from .impl.sanity_checker import SanityChecker

class Saver:
    @classmethod
    def run(cls, dest_path, save_name, dry_run):
        source_path = Utilities.get_save_folder()

        SanityChecker.check(source_path, dest_path, save_name)
        cls._ask_user_confirmation(source_path)
        cls._perform(source_path, dest_path, save_name, dry_run)

    @classmethod
    def _ask_user_confirmation(cls, source_path, dest_path, save_name):
        print(
            clean_multiline(
                f"""
                Saving current saves in '{source_path}' to subfolder '{save_name}' in {dest_path}. Confirm?
                """
            )
        )
        raise NotImplementedError()
        #Get user's response...

    @classmethod
    def _perform(cls, source_path, dest_path, save_name, dry_run):
        final_path = Utilities.subfolder_path(dest_path, save_name)
        Utilities.copy_files(source_path, final_path, dry_run)