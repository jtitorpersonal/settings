class SanityChecker:
    @classmethod
    def check(cls, source_path, dest_path, save_name):
        cls._verify_source_exists(source_path)
        cls._verify_can_write_to_dest(dest_path)
        cls._verify_save_name_not_already_used(dest_path, save_name)

    @classmethod
    def _verify_source_exists(cls, source_path):
        raise NotImplementedError()
    
    @classmethod
    def _verify_can_write_to_dest(cls, dest_path):
        raise NotImplementedError()
    
    @classmethod
    def _verify_save_name_not_already_used(cls, dest_path, save_name):
        raise NotImplementedError()