#ref https://www.pythonmorsels.com/dedent/
from textwrap import dedent

class Utilities:
    @classmethod
    def get_save_folder(cls):
        '''Returns the path to the game's save folder.

        Raises:
            NotImplementedError: _description_
            NotImplementedError: _description_

        Returns:
            _type_: The path to the game's save folder.
        '''
        platform = cls._get_platform()
        
        match platform:
            case "windows":
                return cls._windows_game_save_path()
            case "mac":
                return cls._mac_game_save_path()
            case "linux":
                raise NotImplementedError()
                #raise PlatformNotSupportedError()
            case _:
                raise NotImplementedError()
                #raise UnrecognizedPlatformError()

    @classmethod
    def get_save_list(cls, save_list_path=None):
        '''Returns a list of saves at the given URL, including their status.

        Args:
            save_list_path (_type_): _description_ If None is provided, this will default to `Utilities.get_save_folder()`.

        Returns:
            _type_: The save list. The format is an array of dictionaries; each dictionary has the following keys:
                - "slot": The save's in-game save slot.
                - "paths": Array of URLs to the files corresponding to that save.

        Raises:
            NotImplementedError: _description_
        '''
        if not save_list_path:
            save_list_path = cls.get_save_folder()

        all_filenames = cls._get_filenames(save_list_path)
        save_filenames = all_filenames.filter(r"([^\s\.]*)\.bin").matches(1)
        backup_filenames = all_filenames.filter(r"([^\s\.]*)_backup\.bin").matches(1)
        complete_saves = save_filenames.union(backup_filenames)

        result = []
        for savename in complete_saves:
            save_info = {
                "slot": savename,
                "paths": [save_filenames[save_info], backup_filenames[save_info]]
            }
            result.append(save_info)
            
        return result

    @classmethod
    def ask_if_should_continue(cls, caught_error, ask_message):
        '''Given an incoming error, asks the user if the app should continue
        despite the error. Raises the error if the user declines.

        Args:
            caught_error (_type_): The error that is pausing app execution.
            ask_message (_type_): A string asking the user if they wish to proceed.

        Raises:
            caught_error: If the user does not wish to proceed.
        '''
        print(ask_message)
        if not cls._get_user_wants_to_continue():
            raise caught_error
    
    @classmethod
    def copy_files(cls, source_path, dest_path, dry_run, allow_overwrite = False):
        '''Copies files from one path to another. No files will be modified if an error is raised.

        Args:
            source_path (_type_): The path to copy files from.
            dest_path (_type_): The path to copy files to.
            allow_overwrite (_type_): If False, an error will be raised if any files would be overwritten by the copy operation.
            dry_run (_type_): If True, no files will actually be modified. Good for testing.

        Raises:
            NotImplementedError: _description_
        '''
        raise NotImplementedError()
    
    @classmethod
    def subfolder_path(cls, folder_path, subfolder_name):
        '''Returns a concencation of the folder path and subfolder.

        Args:
            folder_path (_type_): _description_
            subfolder_name (_type_): _description_

        Raises:
            NotImplementedError: _description_
        '''
        raise NotImplementedError()
    
    @classmethod
    def get_default_backup_path(cls):
        '''Returns the default path to the next backup the app can create.

        Returns:
            _type_: The default path to the next backup the app can create.
        '''

        game_save_path = cls.get_save_folder()
        backup_subfolder_name = cls._get_backup_subfolder_name()
        backup_instance_name = cls._get_backup_instance_name()

        result = cls.subfolder_path(game_save_path, backup_subfolder_name)
        result = cls.subfolder_path(result, backup_instance_name)
        return result
    
    #Private methods
    @classmethod
    def _get_platform(cls):
        #Can return: "windows", "mac", "linux"
        raise NotImplementedError()
    
    @classmethod
    def _windows_game_save_path(cls):
        #Expand "~\AppData\LocalLow\nodayshalleraseyou\DeadeyeDeepfakeSimulacrum\"
        raise NotImplementedError()
    
    @classmethod
    def _mac_game_save_path(cls):
        #Expand "~/Library/Application\ Support/com.nodayshalleraseyou.DeadeyeDeepfakeSimulacrum/"
        raise NotImplementedError()
    
    @classmethod
    def _get_filenames(cls):
        raise NotImplementedError()

    @classmethod
    def _get_user_wants_to_continue(cls):
        raise NotImplementedError()
    
    @classmethod
    def _get_backup_subfolder_name(cls):
        #Would be a string constant
        raise NotImplementedError()
        return ""
    
    @classmethod
    def _get_backup_instance_name(cls):
        #Would be a date/time constant,
        #since trying to find the next index in a
        #user-set sequence has tons of edge cases
        raise NotImplementedError()
        return ""

def clean_multiline(multiline_string):
    dedent(multiline_string).strip("\n")