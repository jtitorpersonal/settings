from ..utilities import Utilities

class Lister:
    '''Lists all the saves in a folder.

    By default, this will search the platform-default folder for the game's saves (ref. Utilities.get_save_folder()).
    '''

    def run():
        '''Entry point for the command.
        '''
        save_list = Utilities.get_save_list()
        pass