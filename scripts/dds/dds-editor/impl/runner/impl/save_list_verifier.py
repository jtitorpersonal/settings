from .utilities import Utilities
from .constants import Constants

class SaveListVerifier:
    @classmethod
    def verify_save_files(cls, save_list, ask_message):
        try:
            cls._check_all_saves_valid(save_list)
        except RuntimeError as e:
            cls._report_error_info(e)
            #All files in the load path will be copied to the destination,
            #even if they're missing files
            Utilities.ask_if_should_continue(e, ask_message)

    @classmethod
    def _check_all_saves_valid(cls, save_list):
        all_errors = []

        for save in save_list:
            if not save[Constants.Keys.can_load]:
                all_errors.append(save[Constants.Keys.slot])
        
        if all_errors:
            raise RuntimeError(all_errors)
    
    @classmethod
    def _report_error_info(cls, caught_error):
        error_info = caught_error.args

        print(f"Found {len(error_info)} errors:")
        for error in error_info:
            print(f" - {error}")