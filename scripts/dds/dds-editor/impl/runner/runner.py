from .impl.loader import Loader
from .impl.saver import Saver
from .impl.utilities import Utilities

class Runner:
    @classmethod
    def run(cls):
        loading = cls._get_param_loading()
        dry_run = cls._get_param_dry_run()

        if loading:
            load_path = cls._get_param_load_path()
            backup_path = cls._get_param_backup_path()

            Loader.run(load_path, backup_path, dry_run)
        else:
            save_path = cls._get_param_save_path()
            save_name = cls._get_param_save_name()

            Saver.run(save_path, save_name, dry_run)

    @classmethod
    def _get_param_loading(cls):
        raise NotImplementedError()
        return False
    
    @classmethod
    def _get_param_dry_run(cls):
        raise NotImplementedError()
        return True
    
    @classmethod
    def _get_param_load_path(cls):
        raise NotImplementedError()
        return ""
    
    @classmethod
    def _get_param_backup_path(cls):
        result = cls._get_user_backup_path()
        
        if result:
            return result
        return Utilities.get_default_backup_path()
    
    @classmethod
    def _get_user_backup_path(cls):
        raise NotImplementedError()
        return ""
    
    @classmethod
    def _get_param_save_path(cls):
        raise NotImplementedError()
        return ""
    
    @classmethod
    def _get_param_save_name(cls):
        raise NotImplementedError()
        return ""