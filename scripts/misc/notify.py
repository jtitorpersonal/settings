'''Attempts to place the given notification to the user's
screen.
'''
import argparse
import sys
import os

kMacOsNotifyCommand = "osascript -e 'display notification \"{1}\" with title \"{0}\"'"

def callForPlatform(title, body):
	if sys.platform == "linux" or sys.platform == "linux2":
		raise NotImplementedError("Notifications not implemented for Linux!")
	elif sys.platform == "darwin":
		#MacOS doesn't have a no-body notification.
		os.system(kMacOsNotifyCommand.format(title, body))
	elif sys.platform == "win32":
		raise NotImplementedError("Notifications not implemented for Windows!")

def main():
	parser = argparse.ArgumentParser(description="Attempts to place the given notification to the user's screen.")
	parser.add_argument("title",
						help="The title of the notification.")
	parser.add_argument("-b", "--body", default="",
						help="The body text of the notification. If empty, the script will try to omit this field from the displayed notification.")

	args = parser.parse_args()
	callForPlatform(args.title, args.body)

if __name__ == "__main__":
	main()