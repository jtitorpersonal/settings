#Uses Python 3!
import argparse
import errno
import sys
import glob
import os
import re

from colorama import Fore, Style
from colorama import init

# -Error definitions
class BaseError(Exception):
	"""Base class for exceptions in this module."""
	pass

class InputFileEmptyError(BaseError):
	"""Exception raised when a file could be opened but did not appear to have data to read.

	Attributes:
		filePath -- path to the file that generated this error
		message -- explanation of the error
	"""

	def __init__(self, filePath):
		self.filePath = filePath
		self.message = "File {0} was opened but doesn't appear to have any data when read as a string".format(filePath)

class InputFileNoMatchesError(BaseError):
	"""Exception raised when a file could be opened and contains content,
	but did not have any valid changelist lines.

	Attributes:
		filePath -- path to the file that generated this error
		message -- explanation of the error
	"""

	def __init__(self, filePath):
		self.filePath = filePath
		self.message = "File {0} contains text, but doesn't appear to have any changelist lines".format(filePath)

class InvalidInputError(BaseError):
	"""Exception raised when input to a function doesn't match expected input.

	Attributes:
		message -- explanation of the error
	"""

	def __init__(self, message=""):
		self.message = message

# -Constants.
# --Regex strings.
#Use re.search() to find the positions here.
kDateFormatRegexString = r"\d\d\d\d/\d\d/\d\d \d\d:\d\d:\d\d \w\w"
kChangeListStartRegexString = r"{0}  sd changes -s submitted.*\n".format(kDateFormatRegexString)
kChangeListEndRegexString = r"{0}  \n{0}  \d* changelists found.".format(kDateFormatRegexString)
#$1 = changelist number
#$2 = submission date
#$3 = submission author
#$4 = changelist description
#Use re.findall() to perform this search and replace.
kChangeListLineRegexSearchString = r"Change (\d*) on (.*) by (.*) '(.*)'"
kRegexGroupIndexSubmissionId = 0
kRegexGroupIndexSubmissionDate = 1
kRegexGroupIndexSubmissionAuthor = 2
kRegexGroupIndexSubmissionDescription = 3
kRegexGroupIndexSubmissionGroupsTotal = 4
kChangeListLineRegexReplaceString = "{0}\t{1}\t{2}\t{3}\n"

def changeListMatchToOutputString(match):
	'''Converts an input regex search string to the parsed output string.
	Raises InvalidInputError if the match doesn't have the expected number of match groups.
	'''
	if not match:
		raise InvalidInputError("No input passed to changeListMatchToOutputString()")

	numGroups = len(match)
	if numGroups < kRegexGroupIndexSubmissionGroupsTotal:
		raise InvalidInputError("changeListMatchToOutputString() expects {0} groups in its input, but got {1} group(s); input was '{2}'".format(kRegexGroupIndexSubmissionGroupsTotal, numGroups, match))

	return kChangeListLineRegexReplaceString.format(match[kRegexGroupIndexSubmissionId], match[kRegexGroupIndexSubmissionDate], match[kRegexGroupIndexSubmissionAuthor], match[kRegexGroupIndexSubmissionDescription])

# --File types.
kFileTypeTxt = "*.txt"
kFileTypeLog = "*.log"

# --Flag constants.
#Positional parameter 1.
kFlagInDirectoryName = "inDirectory"
kFlagInDirectoryDescription = "Path to the file containing the raw log files ('{0}' extension).".format(kFileTypeTxt)
#Positional parameter 2.
kFlagOutFileName = "outFile"
kFlagOutFileDescription = "Path to the file where the parsed content will be saved. Any nonexistent subdirectories in the path will be created."

# --Colorama constants.
kErrorFormatChar = Fore.RED
kWarningFormatChar = Fore.YELLOW
kInfoFormatChar = Style.BRIGHT
kSuccessFormatChar = Fore.GREEN
#Verbose text can just be plain text
kVerboseFormatChar = ""
kResetFormatChar = Style.RESET_ALL
#kStdOutStream = AnsiToWin32(sys.stdout).stream

# --Return codes.
kReturnSuccess = 0
kReturnPartialSuccess = 2
kReturnFailure = 1

# -Logging functions.
def log(text, formatChar):#, stream=kStdOutStream):
	print("{0}{1}{2}\n".format(formatChar, text, kResetFormatChar))#, file=kStdOutStream)

def logInfo(text):
	log(text, kInfoFormatChar)

def logWarning(text):
	log(text, kWarningFormatChar)

def logError(text):
	log(text, kErrorFormatChar)

def logSuccess(text):
	log(text, kSuccessFormatChar)

def logVerbose(text):
	log(text, kVerboseFormatChar)

# -Filesystem functions.
def makeSubdirs(filePath):
	finalFilePath = os.path.dirname(filePath)
	if not os.path.exists(finalFilePath):
		try:
			os.makedirs(os.path.dirname(finalFilePath))
		except OSError as exc: # Guard against race condition
			if exc.errno != errno.EEXIST:
				raise

# -Main execution functions.
def parseSingleFile(inFilePath, outFile):
	try:
		with open(inFilePath, "r") as inFile:
			#Pull the file's data, and sanity check that
			#it's a scannable string.
			inputFileFullString = inFile.read()

			if not inputFileFullString:
				kMatchFailedString = "File doesn't appear to have any file contents, can't parse!"
				raise InputFileEmptyError(inFilePath)

			#Search the input file for the CL start and end.
			#If either can't be found, early out.
			changeListStartMatch = re.search(kChangeListStartRegexString, inputFileFullString)
			if not changeListStartMatch:
				kMatchFailedString = "File doesn't appear to have the changelist prelude line, can't parse!"
				raise InputFileEmptyError(inFilePath)

			changeListEndMatch = re.search(kChangeListEndRegexString, inputFileFullString)
			if not changeListEndMatch:
				kMatchFailedString = "File doesn't appear to have the changelist postscript line, can't parse!"
				raise InputFileEmptyError(inFilePath)

			#If both positions are found, get the substring between them.
			changeListStartPosition = changeListStartMatch.end()
			changeListEndPosition = changeListEndMatch.start()
			
			changeListSubstring = inputFileFullString[changeListStartPosition:changeListEndPosition]
			
			#Search for all CL entries. If the list of found CLs is nonempty, start working.
			changeListMatches = re.findall(kChangeListLineRegexSearchString, changeListSubstring)
			if changeListMatches:
				#For each CL found:
				for match in changeListMatches:
					#Write the replacement line into the output file.
					try:
						parsedString = changeListMatchToOutputString(match)
						outFile.write(parsedString)
					except InvalidInputError as ex:
						#If the line fails, report error and keep going
						kParseFailedString = "Failed to parse a line: {0}".format(ex.message)
						logError(kParseFailedString)

				#We're done!
			else:
				#No matches found, report error
				kMatchFailedString = "Couldn't find any changelist entries in file!"
				logError(kMatchFailedString)
				raise InputFileNoMatchesError(inFilePath)
	except FileNotFoundError:
		kFileNotFoundString = "File '{0}' doesn't exist!".format(inFilePath)
		logError(kFileNotFoundString)
		raise

def parseAllFilesInDirectory(inDirectoryPath, outFilePath):
	#Make sure we can open the output file first.
	try:
		#Open in append mode.
		#Create subdirectories if needed.
		makeSubdirs(outFilePath)
		with open(outFilePath, "a") as outFile:
			logVerbose("Opened file {0}".format(outFilePath))

			#Get the list of valid files (for now assume .txt extension):
			txtFilesInDirectory = None
			try:
				os.chdir(inDirectoryPath)
				txtFilesInDirectory = glob.glob(kFileTypeTxt)
			except:
				kDirectoryListFailedString = "Couldn't list files in directory '{0}'!\nReason: {1}".format(inDirectoryPath, sys.exc_info())
				logError(kDirectoryListFailedString)
				raise

			numberOfTxtFiles = len(txtFilesInDirectory)
			numberOfTxtFilesParsed = 0
			logVerbose("Found {0} files to read".format(numberOfTxtFiles))

			#For each .txt file:
			for txtFilePath in txtFilesInDirectory:
				try:
					#Perform parsing.
					parseSingleFile(txtFilePath, outFile)
					numberOfTxtFilesParsed += 1
				except BaseError:
					#If we encounter a failure here, log that something's gone wrong with this file,
					#and continue with the next.
					kFileParseFailedString = "Failed to parse file '{0}', skipping!".format(txtFilePath)
					logError(kFileParseFailedString)

			if numberOfTxtFilesParsed == numberOfTxtFiles:
				logSuccess("All files parsed.")
				return kReturnSuccess
			elif numberOfTxtFilesParsed > 0:
				logWarning("Parsed {0} of {1} files.".format(numberOfTxtFilesParsed, numberOfTxtFiles))
				return kReturnPartialSuccess
			else:
				logError("No files parsed!")
				return kReturnFailure
	except:
		#Report why the file couldn't be opened.
		kFileOpenFailedString = "Couldn't open output file '{0}'!\nReason: {1}".format(outFilePath, sys.exc_info())
		logError(kFileOpenFailedString)
		raise

def doParse():
	parser = argparse.ArgumentParser(description='Process some integers.')
	parser.add_argument(kFlagInDirectoryName,
						help=kFlagInDirectoryDescription)
	parser.add_argument(kFlagOutFileName,
						help=kFlagOutFileDescription)

	return parser.parse_args()

def main():
	#Start Colorama.
	init()

	#Parse out the flags for input and output.
	args = doParse()

	inDirectoryPath = args.inDirectory
	outFilePath = args.outFile

	returnCode = 0
	try:
		returnCode = parseAllFilesInDirectory(inDirectoryPath, outFilePath)
	except:
		#Called functions will report details of errors,
		#so just report general failure here.
		kParseFailedString = "Failed to parse directory '{0}'!".format(inDirectoryPath)
		logError(kParseFailedString)
		returnCode = kReturnFailure

	sys.exit(returnCode)

if __name__ == "__main__":
	main()
