# System Layout
The system consists of distinct virtual or physical machines, called *nodes* in this file.
## Controller Node
A physical or VM node that directs the testing process. This node sets up the tester nodes in `/tester` and schedules their sync/build/test commands. The controller node also performs final reporting of test results to the admin and subscribed users.
## Tester Node
Typically a VM controlled by the controller node in `/controller`.
This is meant to load a specific version of macOS, then run unit/UI tests on an Outlook build.

# Design Scopes
There are different variations on the way the system can be implemented internally,
listed in `/scopes` by order of increasing complexity:
1. Controller sets up testers. Testers sync, build, and test.
2. Controller sets up testers and syncs. Testers build from the controller's source and test.
3. Controller sets up testers, syncs and performs build. Testers only run tests on controller's build.