# S2: Build on Controller
The tester runs XCode on the controller's copy of the sources. To perform the test, the tester-controller system must:
1. Sync source
2. Build source
3. Test source

Now only the controller needs credentials,
but if it loses connection or fails for any reason to sync the entire system can't test.

## Provision
### Controller
#### 1: Required User Input
The following must be provided or provisioning will abort:
* User credentials:
	* Required:
		* Alias
		* Password
	* Optional until S2 Implemented:
		* Domain
#### 2: Content Copy
The following must be copied to the tester:
* XCode installer
* ApexSetup
#### 3: Tool Installation
Next, software is installed:
* XCode
* XCode Terminal Tools
* ApexSetup
* Initial source sync: `sd sync`
* If at any point an installer fails:
	* Provisioning has failed. Destroy the controller.
	* Abort here.

### Tester
#### 1: Required User Input
The following must be provided or provisioning will abort:
* User credentials to access controller's source repo:
	* Required:
		* Username
		* Password
For each tester:
#### 2: Content Copy
The following must be copied to the tester:
* XCode installer
#### 3: Tool Installation
Next, software is installed:
* XCode
* XCode Terminal Tools
* If at any point an installer fails:
	* Provisioning has failed. Destroy all testers.
	* Destroy the controller.
	* Abort here.

## Repeated Steps
These steps are repeated at least daily.
## 1: Controller Sync
The controller performs:
* `kinit (user) (credentials)` to reset ticket if it's been over 12 hours
* `mbusd update` to sync to the repo head.
	* TODO: Specify flags for mbusd update to sync directly to head.
	The call shouldn't need a subsequent resolve.
	* If any sync errors are found:
		* Report sync failure:
			* Send email to Build Failure email list:
				* Subject: "Tester (tester type): Sync Failed @ (time)".
				* Body should contain:
					* Human-readable summary of:
						* Which step failed
						* Why that step appears to have failed
					* Raws of the terminal output
		* Abort repeated steps.
## 2: Local Build
Next, each tester performs:
* For each configuration `config`:
	* `mbusd build -d outlook (config)` to build all Outlook configurations.
		* If the invocation fails:
			* Add tester and configuration to list of failed builds.
			* Save raws of the terminal output, as well as the failed step.
		* Else:
			* Add tester and configuration to list of testable builds.
## 3: Test
Finally, each tester performs:
* For each configuration `config`:
	* `mbusd test outlook (config)` to test all Outlook configurations.
		* If the invocation fails:
			* Add tester and configuration to list of failed tests.
			* Save raws of the terminal output, as well as the failed step.
		* Else:
			* Add tester and configuration to list of passed tests.
* If `failedConfigs` is not empty:
	* Send email to Build Failure email list:
		* Subject: "Tester (tester type): Test for (failedConfigs length) Configuration(s) Failed @ (time)".
		* Body should contain:
			* Human-readable summary of:
				* Which configurations failed
			* Raws of the terminal output for each configuration in `failedConfigs`.
* Else:
	* Send email to Build Success email list:
		* Subject: "Tester (tester type): All Tests Passed @ (time)"
		* Body can be any congratulations message, possibly timing metrics.