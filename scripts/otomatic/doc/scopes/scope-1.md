# Scope 1: Build on Tester
The tester is itself a Apex enlistment. To perform its test, it must:
1. Sync source
2. Build source
3. Test source

This isn't efficient (*every* tester needs credentials), but is simple to understand and implement.

## Provision
### 1: Required User Input
The following must be provided or provisioning will abort:
* User credentials:
	* Required:
		* Alias
		* Password
	* Optional until S1 Implemented:
		* Domain
### 2: Content Copy
The following must be copied to the tester:
* XCode installer
* ApexSetup
### 3: Tool Installation
Next, software is installed:
* XCode
* XCode Terminal Tools
* ApexSetup
* Initial source sync: `sd sync`
* If at any point an installer fails:
	*  Provisioning has failed. Destroy the tester.

## Repeated Steps
These steps are repeated at least daily.
## 1: Local Sync
The tester performs:
* `kinit (user) (credentials)` to reset ticket if it's been over 12 hours
* `mbusd update` to sync to the repo head.
	* TODO: Specify flags for mbusd update to sync directly to head.
	The call shouldn't need a subsequent resolve.
	* If any sync errors are found:
		* Report sync failure:
			* Send email to Build Failure email list:
				* Subject: "Tester (tester type): Sync Failed @ (time)".
				* Body should contain:
					* Human-readable summary of:
						* Which step failed
						* Why that step appears to have failed
					* Raws of the terminal output
		* Abort repeated steps.
## 2: Local Build
Next, the tester performs:
* For each configuration `config`:
	* `mbusd build -d outlook (config)` to build all Outlook configurations.
		* If the invocation fails:
			* Report build failure:
				* Send email to Build Failure email list:
					* Subject: "Tester (tester type): Build for (config) Failed @ (time)".
					* Body should contain:
						* Human-readable summary of:
							* Which configuration failed
						* Raws of the terminal output
		* Abort repeated steps.
## 3: Test
Finally, the tester performs:
* For each configuration `config`:
	* `mbusd test outlook (config)` to test all Outlook configurations.
		* If the invocation fails:
			* Add `config` to list of failed configurations `failedConfigs`.
* If `failedConfigs` is not empty:
	* Send email to Build Failure email list:
		* Subject: "Tester (tester type): Test for (failedConfigs length) Configuration(s) Failed @ (time)".
		* Body should contain:
			* Human-readable summary of:
				* Which configurations failed
			* Raws of the terminal output for each configuration in `failedConfigs`.
* Else:
	* Send email to Build Success email list:
		* Subject: "Tester (tester type): All Tests Passed @ (time)"
		* Body can be any congratulations message, possibly timing metrics.