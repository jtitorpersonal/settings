# Overview
Automated system for running XCode unit tests and UI tests.

# Installing
## Install Control Programs
You'll need Vagrant to control the sample controller box and the tester box. This can be done on any platform,
but for macOS it's easiest to install Brew, then install Vagrant and VirtualBox through Brew:
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew cask install virtualbox
brew cask install vagrant
```

## Controlling Boxes
Once that's done, you can try the different node types through SSH by going to `/controller` or `/tester` and running:
```
vagrant up && vagrant ssh
```

By convention, the login for all Vagrant boxes is:
* Username: `vagrant`
* Password: `vagrant`

Because of this, *do not set any of the Vagrantfiles to use a public network if possible*.

To shutdown the node, run `vagrant destroy -f` while in the directory of that node (`/controller` or `/tester`).
You can also run `vagrant destroy -f [box id]` if you know the node's box id from `vagrant global-status`.

# How to Use
TODO