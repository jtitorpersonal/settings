'''Defines printing functions used by the program.
'''
from datetime import datetime
import os

#ANSI color codes.
kPython3Escape = "\u001b"
kPython2Escape = "\033"
kAnsiRed = "{0}[31m".format(kPython2Escape)
kAnsiGreen = "{0}[32m".format(kPython2Escape)
kAnsiBold = "{0}[1m".format(kPython2Escape)
kAnsiReset = "{0}[0m".format(kPython2Escape)

kLogFileNameFormatString = "precheckin_test.{0}.log"

#Logging functions.
class Printer(object):
	'''Handles printing text to standard output. Also logs text to 
	a log file.
	'''

	def __init__(self, enableVerbose=False):
		'''Instance initializer.

		:param bool enableVerbose: If False, printVerbose() calls will not be displayed or logged.
		'''

		#If False, printVerbose() calls will do nothing
		self.enableVerbose = enableVerbose

		#Setup the log file
		logFileName = kLogFileNameFormatString.format(datetime.now().strftime("%Y.%m.%d-%H.%M.%S"))
		logPath = os.path.realpath(os.path.expanduser("~/Library/Containers/com.microsoft.Outlook/Data/Library/Logs/{0}".format(logFileName)))
		#The log file storing the printer's printed text.
		self.logFile = open(logPath, "a+")

	def __del__(self):
		'''Instance deallocator.
		'''
		self.logFile.close()

	def _printWithAnsi(self, text, ansiCode):
		'''Prints the given text, prefacing it with the specified ANSI escape code
		and suffixing it with an ANSI reset format escape code. Also logs the given
		text to the printer's log file.
		
		:param str text: The text to be printed.
		:param str ansiCode: The ANSI escape code to preface the text with.
		'''
		print("{0}{1}{2}".format(ansiCode, text, kAnsiReset))
		self.logFile.write("{0}\n".format(text))

	def printError(self, text):
		'''Prints the specified text to the screen as an error message.

		:param str text: The text to be printed.
		'''
		self._printWithAnsi(text, kAnsiRed)

	def printSuccess(self, text):
		'''Prints the specified text to the screen as a success message.

		:param str text: The text to be printed.
		'''
		self._printWithAnsi(text, kAnsiGreen)

	def printInfo(self, text):
		'''Prints the specified text to the screen as an information message.

		:param str text: The text to be printed.
		'''
		self._printWithAnsi(text, kAnsiBold)

	def printShellOutput(self, text):
		'''Prints the specified text to the screen as output from a shell command.

		:param str text: The text to be printed.
		'''
		self._printWithAnsi(text, kAnsiReset)

	def printVerbose(self, text):
		'''Prints the specified text to the screen as a verbose message.
		The message will only be displayed/logged if enableVerbose is True.

		:param str text: The text to be printed.
		'''
		if self.enableVerbose:
			self._printWithAnsi(text, kAnsiReset)