'''Performs automation tests on Outlook meant to be done before checkin of changes.
'''
from datetime import datetime
import glob
import os
import re
import sys

from .printing import Printer
from .errors import *
from . import shell_commands

#Abort right here if we're not in macOS somehow,
#since none of these commands/paths will be valid otherwise
if sys.platform != "darwin":
	print("This script requires macOS to run.")
	exit(1)

#Path constants.
#Path to the Office group container.
kOfficeGroupContainerPath = os.path.realpath(os.path.expanduser("~/Library/Group Containers/UBF8T346G9.Office"))
#Path to the Outlook group container in the Office group container.
kOutlookGroupContainerPath = "{0}/Outlook".format(kOfficeGroupContainerPath)
kBackupFolderNameRegex = r"Outlook\..*\.backup"
#Date-time string used to generate a backup directory for this run.
sCurrentDateTimeString = datetime.now().strftime("%Y.%m.%d-%H.%M.%S")
#Default backup path is Outlook.[current datetime].backup
sDefaultBackupPath = "{0}.{1}.backup".format(kOutlookGroupContainerPath, sCurrentDateTimeString)

#List of automation scenario ids to be tested.
kScenarioIdsString = "405933,405151"

#Enum for program status.
kResultSuccess = 0
kResultBackupFailed = 1
kResultTestsFailed = 2
kResultBackupRestoreFailed = 3
kResultNotStarted = 4

#Main program flow.
class Runner(object):


	def __init__(self, enableVerbose=False):
		'''Instance initializer.
		'''
		self._printer = Printer(enableVerbose=enableVerbose)

	def _runShellCommand(self, commandList):
		'''Runs the given list of shell commands,
		printing shell output to standard output.

		:returns: The error code of the command that was run.
		'''
		commandInvocation = " ".join(commandList)
		self._printer.printVerbose("Running command: {0}".format(commandInvocation))
		
		#By default, print the command output to standard output
		commandResult = shell_commands.runShellCommand(commandList)
		
		self._printer.printShellOutput(commandResult.output)
		if commandResult.exitCode != 0:
			self._printer.printError("Command failed: {0}".format(commandInvocation))

		return commandResult.exitCode

	def _backupProfile(self):
		'''Backs up any existing Outlook profile by moving it to a temporary folder.

		:raises BackupFailedError: If the backup failed to be created for some reason.
		'''
		self._printer.printInfo("Backing up profile...")
		if os.path.exists(kOutlookGroupContainerPath):
			self._printer.printInfo("Backing up Outlook profile at '{0}'...".format(kOutlookGroupContainerPath))
			backupCommand = shell_commands.moveCommandList(kOutlookGroupContainerPath, sDefaultBackupPath)
			if self._runShellCommand(backupCommand) != 0:
				raise BackupShellCommandFailed(backupCommand)
		else:
			self._printer.printInfo("No Outlook profile found at '{0}', skipping profile backup".format(kOutlookGroupContainerPath))

	def _findBackupSubdirectories(self, rootDirectory):
		'''Returns a list of paths in the specified root directory
		that match the backup folder naming scheme and are not empty.
		'''
		backupPaths = os.listdir(rootDirectory)
		filterRegex = re.compile(kBackupFolderNameRegex)
		#Filter by backup suffix, then by checking
		#that the path is a directory that's not empty
		backupPaths = [path for path in backupPaths if (filterRegex.match(path) and os.path.isdir(path) and os.listdir(path))]
		
		return backupPaths

	def _findMostRecentBackup(self):
		'''Finds the path to the most recent backup, if it exists.

		:returns: The most recent backup if it exists.
		:raises NoBackupsFoundError: If no backup folder was found.
		'''
		#If the default path exists, return that
		if os.path.exists(sDefaultBackupPath):
			return sDefaultBackupPath

		#Otherwise, search for paths fitting the backup folder format.
		backupRootFolder = kOfficeGroupContainerPath
		backupPaths = self._findBackupSubdirectories(kOfficeGroupContainerPath)

		#If no backup paths exist, that's an error
		if not backupPaths:
			self._printer.printError("No backups found in '{0}'".format(backupRootFolder))
			raise NoBackupsFoundError(backupRootFolder)

		#Sort by descending name, this'll get the most recent folder.
		backupPaths = sorted(backupPaths, reverse=True)
		#Return that folder's path.
		return backupPaths[0]

	def _runTests(self):
		'''Runs the test scenarios on Outlook.

		:raises TestFailedError: If the "mbu test" invocation failed for any reason
		'''
		self._printer.printInfo("Running automation tests...")
		runTestsCommand = ["mbu", "test", "-scenarioid", kScenarioIdsString, "-c", "debug"]

		if self._runShellCommand(runTestsCommand) != 0:
			raise TestFailedError()

	def _restoreProfile(self):
		'''Restores any existing Outlook profile by moving it from a temporary folder
		to the Outlook profile folder.
		'''
		self._printer.printInfo("Restoring profile...")

		#Check that we have a backup to restore
		backupPath = ""
		try:
			backupPath = self._findMostRecentBackup()
			self._printer.printVerbose("Found profile backup folder at {0}".format(backupPath))

			#Check the path exists, just in case
			if not os.path.exists(backupPath):
				self._printer.printError("Backup folder '{0}' doesn't exist, can't restore profile".format(backupPath))
				raise BackupDoesNotExistError(backupPath)
		except BackupError:
			#If there's no backup, bubble the error up and early out here,
			#since we can't restore nothing
			raise

		#Finally, check if the profile folder exists and is not empty so we don't clobber
		if os.path.exists(kOutlookGroupContainerPath) and os.listdir(kOutlookGroupContainerPath):
			self._printer.printInfo("Outlook profile folder '{0}' already exists! Overwriting with backup from '{1}'".format(kOutlookGroupContainerPath, backupPath))
			deleteCommand = shell_commands.deleteCommandList(kOutlookGroupContainerPath)
			if self._runShellCommand(deleteCommand) != 0:
				raise BackupShellCommandFailed(deleteCommand)

		#Move the backup back in place
		moveCommand = shell_commands.moveCommandList(backupPath, kOutlookGroupContainerPath)
		self._printer.printVerbose("Restoring backup from {0} to {1}...".format(backupPath, kOutlookGroupContainerPath))
		if self._runShellCommand(moveCommand) != 0:
			raise BackupShellCommandFailed(moveCommand)

		#Now unlock the backup and delete it
		deleteCommand = shell_commands.deleteCommandList(backupPath)
		self._printer.printVerbose("Backup restored, removing backup folder {0}".format(backupPath))
		if self._runShellCommand(deleteCommand) != 0:
			raise BackupShellCommandFailed(deleteCommand)

	def run(self):
		'''Primary entry point for the program.

		:returns: Exit code of 0 if the automation tests were successful, 1 otherwise.
		'''
		result = kResultNotStarted
		try:
			# Back up existing Outlook profile, if any
			self._backupProfile()

			# Run the two tests
			self._runTests()

			result = kResultSuccess
		except BackupError:
			self._printer.printError("Backup failed, aborting test!")
			result = kResultBackupFailed
		except TestFailedError:
			self._printer.printError("Tests failed!")
			result = kResultTestsFailed
		finally:
			#Always try to restore the backup if there is one
			try:
				#Restore our profile
				self._restoreProfile()
			except BackupError:
				self._printer.printError("Backup restore failed! Profile may be in an invalid state!")
				result = kResultBackupRestoreFailed

		#Now finally return our return code.
		if result == kResultSuccess:
			self._printer.printSuccess("Tests complete!")
			exit(0)
		else:
			exit(1)
