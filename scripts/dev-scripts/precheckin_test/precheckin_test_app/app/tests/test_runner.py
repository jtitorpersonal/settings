'''Defines the class that runs all unit tests in the program.
'''

import unittest

from . import printing
from . import shell_commands

class TestRunner(object):
	'''Runs all unit tests in the application.
	'''

	def __init__(self):
		'''Instance initializer.
		'''
		self._loader = unittest.TestLoader()
		self._suite = unittest.TestSuite()

		# add tests to the test suite
		self._suite.addTests(self._loader.loadTestsFromModule(printing))
		self._suite.addTests(self._loader.loadTestsFromModule(shell_commands))

	def run(self):
		'''Runs all unit tests retrieved by this TestRunner.
		'''

		# initialize a runner, pass it your suite and run it
		runner = unittest.TextTestRunner(verbosity=3)
		result = runner.run(self._suite)

		return result