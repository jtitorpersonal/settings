'''Defines unit tests for the shell_commands module.
'''
import unittest

from .. import shell_commands

class TestShellCommands(unittest.TestCase):

	def testMoveCommand(self):
		'''Tests that shell_commands.moveCommandList() generates the correct shell command.
		'''

		kSourceDirectory = "source"
		kDestinationDirectory = "destination"
		kExpectedCommandString = "mv -f {0} {1}".format(kSourceDirectory, kDestinationDirectory)
		self.assertEqual(" ".join(shell_commands.moveCommandList(kSourceDirectory, kDestinationDirectory)), kExpectedCommandString)

	def testDeleteCommand(self):
		'''Tests that shell_commands.deleteCommandList() generates the correct shell command.
		'''

		kTargetDirectory = "target"
		kExpectedCommandString = "rm -rf {0}".format(kTargetDirectory)
		self.assertEqual(" ".join(shell_commands.deleteCommandList(kTargetDirectory)), kExpectedCommandString)

	def testRunShellCommandSuccess(self):
		'''Tests that a successful shell command via shell_commands.runShellCommand()
		does return a zero exit code.
		'''

		#This command should return a zero exit code
		kCommandShouldSucceed = ["echo", "test"]
		commandResult = shell_commands.runShellCommand(kCommandShouldSucceed)
		print(commandResult.output)
		self.assertEqual(commandResult.exitCode, 0)

	def testRunShellCommandFailure(self):
		'''Tests that an unsuccessful shell command via shell_commands.runShellCommand()
		does return a nonzero exit code.
		'''

		#This command should return a nonzero exit code
		kCommandShouldFail = ["ls", "189z3nuyuh"]
		commandResult = shell_commands.runShellCommand(kCommandShouldFail)
		print(commandResult.output)
		self.assertNotEqual(commandResult.exitCode, 0)
