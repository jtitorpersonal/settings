'''Defines unit tests for the Printer class.
'''
import unittest

from ..printing import Printer

class TestPrinter(unittest.TestCase):
	def testPrinting(self):
		'''Calls all methods of Printer to demonstrate that their formatting
		renders correctly.
		'''

		#Just display all types of text
		printer = Printer(enableVerbose=True)
		printer.printError("Error text")
		printer.printInfo("Info text")
		printer.printSuccess("Success text")
		printer.printVerbose("Verbose text")
		printer.printShellOutput("Shell output text")