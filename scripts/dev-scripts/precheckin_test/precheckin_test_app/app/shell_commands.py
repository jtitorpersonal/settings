'''Defines shell commands and shell interaction functions.
'''
import subprocess

#Command constants.
kMoveCommandList = ["mv", "-f"]
def moveCommandList(source, destination):
	return kMoveCommandList + [source, destination]

kDeleteCommandList = ["rm", "-rf"]
def deleteCommandList(target):
	return kDeleteCommandList + [target]

class ShellCommandResult(object):
	'''Contains the text output of a shell command,
	as well as the exit code of the command.
	'''
	def __init__(self, output, exitCode):
		self.output = output
		self.exitCode = exitCode

def runShellCommand(commandList):
	'''Runs the given list of shell commands,
	printing shell output to standard output.

	:returns: A ShellCommandResult object containing the command's
	exit code and output.
	'''
	output = ""
	errorCode = 0
	try:
		#Actually run the command.
		#Print standard error to output too, so we see if anything breaks.
		output = subprocess.check_output(commandList, stderr=subprocess.STDOUT, universal_newlines=True)
	except subprocess.CalledProcessError as ex:
		output = ex.output
		errorCode = ex.returncode

	#Now return the command error code and text output.
	return ShellCommandResult(output, errorCode)