'''Defines errors used by the program.
'''

#Internal error types.
class BaseError(Exception):
	'''Base class for all errors in this application.
	'''
	pass

class BackupError(BaseError):
	'''Base class for all errors related to backups.
	'''
	pass

class BackupShellCommandFailed(BackupError):
	'''Error indicating a shell command used during the
	backup process failed to run properly.
	'''
	def __init__(self, command):
		#The command that failed to run correctly.
		self.command = command

class NoBackupsFoundError(BackupError):
	'''Error indicating no backup directories were found in a given directory.
	'''
	def __init__(self, rootDirectorySearched):
		#The root directory being searched for backups.
		self.rootDirectorySearched = rootDirectorySearched

class BackupDoesNotExistError(BackupError):
	'''Error indicating that a backup directory does not exist
	despite searches indicating otherwise.
	'''
	def __init__(self, invalidBackupPath):
		#The backup directory that was supposed to exist, but doesn't.
		self.invalidBackupPath = invalidBackupPath

class BackupWouldOverwriteProfileError(BackupError):
	'''Error indicating that a profile already exists,
	so a backup restore would overwrite user data. 
	'''
	def __init__(self, backupPath, profilePath):
		#The backup directory that was going to be used
		#for the restore operation.
		self.backupPath = backupPath
		#The profile directory that was supposed to be empty,
		#but contains profile data.
		self.profilePath = profilePath

class TestFailedError(BaseError):
	'''Error indicating that the automation tests failed.
	'''
	pass