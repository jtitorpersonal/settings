'''Defines the primary entry point for this program.
'''

import argparse

from .app import Runner, TestRunner

sEnableVerbose = False

def runApp():
	Runner(sEnableVerbose).run()

def runUnitTests():
	TestRunner().run()

def main():
	'''Entry point function.
	'''
	global sEnableVerbose
	
	#Set up the main parser
	parser = argparse.ArgumentParser(prog='PROG')
	parser.add_argument('--verbose', action='store_true', help='If set, enables verbose logging.')
	parser.add_argument('--unittests', action='store_true', help='If set, runs unit tests.')

	args = parser.parse_args()
	sEnableVerbose = args.verbose
	#Now dispatch to our subcommands:
	if args.unittests:
		#Go to unit tests if --unittests was passed
		runUnitTests()
	else:
		#Otherwise, run the pre-checkin tests
		runApp()

if __name__ == "__main__":
	main()