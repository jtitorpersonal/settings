#!/bin/bash
#Setup colors.
kRed=`tput setaf 1`
kGreen=`tput setaf 2`
kBold=`tput bold`
kReset=`tput sgr0`

if ! kinit mileun@NORTHAMERICA.CORP.MICROSOFT.COM
then
	echo "${kRed}Kerberos login failed, aborting.${kReset}"
	exit 1
fi

echo "${kBold}Updating local sources...${kReset}"
#Never sync to hot if you can help it!
#'osync at current' syncs to last stable build (usually ~2 weeks old),
#'osync at latest' apparently syncs to last good build (<1 day old)
if ! osync at latest
then
	#mbusd update doesn't report success
	#if any files had to be merged; run it again in that case.
	if ! osync at latest
	then
		echo "${kRed}Update failed, aborting.${kReset}"
		exit 1
	fi
fi

echo "${kBold}Resolving conflicts...${kReset}"
if ! sd resolve
then
	echo "${kRed}Resolve failed.${kReset}"
	exit 1
fi

echo "${kGreen}Morning startup complete!${kReset}"