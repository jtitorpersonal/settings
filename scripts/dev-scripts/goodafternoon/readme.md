# Summary
Performs multiple types of commands:
  * Syncing to most recent good build (ocheck -fix; osync_apple sync; sd resolve)
  * Building/nuking to get a good build
  * Finding lurkers and automatically staging them
  * Finding targets that need building/testing according to validatechanges and performing the respective required commands

While doing this, it also prints what's going on to:
  * Standard output
  * A log file (mentioned in the standard output and again if a command fails)
  * Optionally, a Teams bot

# Usage
```goodafternoon [command]```, where ```[command]``` is one of the following:
  * ```build```
    * Builds the project, performing a nuke if needed.
  * ```prepsubmit```
    * Finds targets that need building/testing according to ```mbu validatechanges``` and performs the respective required commands.
  * ```stagelurkers```
    * Finds lurkers and automatically stages them.
  * ```submit```
    * Performs ```goodmorning prepsubmit```, then attempts to submit the given changelist.
  * ```sync```
    * Syncs to most recent good build.
  * ```test```
    * Runs unit tests on Goodmorning.

# Distributing
You'll need to make a .tar.gz to install on the destination system. Make the .tar with ```python setup.py sdist```, then install the .tar with ```pip install [path to .tar]```.