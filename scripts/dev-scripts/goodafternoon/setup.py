'''Setup file for goodafternoon package.
'''
from setuptools import setup, find_packages

setup(
	# Application name:
	name="olkmac_goodafternoon",
	# Version number (initial):
	version="0.1.5",
	# Application author details:
	author="Michael Leung",
	author_email="mileun@microsoft.com",
	# Packages
	packages=find_packages(),
	# Include additional files into the package
	include_package_data=True,
	# Details
	url="",
	license="",
	description=("Performs commonly-used mbu/SD commands."),
	#long_description=open("readme.md").read(),
	# Dependent packages (distributions)
	install_requires=[
		"colorama",
		"jtitor_basis"
	],
	entry_points={
		'console_scripts': ['goodafternoon=goodafternoon.goodafternoon:main']
	}
)
