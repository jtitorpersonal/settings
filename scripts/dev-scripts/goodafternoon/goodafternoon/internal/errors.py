'''Defines errors used by goodafternoon's
code.
'''

class Error(Exception):
	'''Base class for exceptions used by
	goodafternoon.'''
	pass

class ParameterInvalidError(Error):
	'''Exception raised when a parameter to a
	command is invalid.

	Attributes:
		message -- explanation of the error
	'''

	def __init__(self, message):
		'''Initializer method.'''
		Error.__init__(self)

		self.message = message
