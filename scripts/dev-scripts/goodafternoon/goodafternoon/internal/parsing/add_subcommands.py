'''Defines the addSubcommands() function.
'''
import argparse
import sys

from basis import Basis

from ..commands import Build, PrepSubmit, StageLurkers, Submit, Sync
from ..test import TestRunner

class Runner(object):
	'''Parses invocation flags and dispatches to the necessary subcommand.
	'''
	def __init__(self):
		# basis.installStartMessage = "Starting build/validate..."
		# basis.checkStartMessage = "Checking build/validate status (this is kind of redundant)..."
		# basis.installFailedMessage = "Build/validate failed on the following steps"
		# basis.successMessage = "Build/validate complete, ready for submit"
		self.basis = Basis(args="")
		self.args = None
		self.parser = createSubcommandParser(self)

	def build(self):
		Build(self.basis).run()

	def prepSubmit(self):
		PrepSubmit(self.basis).run()

	def stageLurkers(self):
		StageLurkers(self.basis).run()

	def submit(self):
		Submit(self.basis, self.args.changelist).run()

	def sync(self):
		Sync(self.basis).run()

	def unittest(self):
		TestRunner().run()

	def parseArgsAndRun(self):
		'''Runs the runner's parser and calls the respective subcommand.
		'''
		self.args = self.parser.parse_args()
		print(self.args)
		errCode = self.args.func()
		sys.exit(errCode)

class SubcommandDefinition(object):
	def __init__(self, commandName: str, helpText: str, function):
		self.commandName = commandName
		self.helpText = helpText
		self.function = function

def _initSubcommandParser(subparser: argparse.ArgumentParser, definition: SubcommandDefinition):
	result = None
	if definition.helpText:
		result = subparser.add_parser(definition.commandName, help=definition.helpText)
	else:
		result = subparser.add_parser(definition.commandName)
	
	result.set_defaults(func=definition.function)
	
	return result

kCommandNameBuild = "build"
kCommandNamePrepSubmit = "prepsubmit"
kCommandNameStageLurkers = "stagelurkers"
kCommandNameSubmit = "submit"
kCommandNameSync = "sync"
kCommandNameUnitTest = "unittest"

def createSubcommandParser(runner: Runner):
	'''Creates an ArgParse parser that parses all subcommands.

	:returns: An ArgParse parser that parses all subcommands.
	'''
	parser = argparse.ArgumentParser(prog="PROG")
	subparsers = parser.add_subparsers(help="sub-command help", dest="cmd")
	subparsers.required = True
	subcommandParsers = {}
	allSubcommands = [
		SubcommandDefinition(kCommandNameBuild, "Syncs and builds the default outlook project.", runner.build),
		SubcommandDefinition(kCommandNamePrepSubmit, "Prepares the enlistment for changelist submission.", runner.prepSubmit),
		SubcommandDefinition(kCommandNameStageLurkers, "Finds any unmarked changes and marks them accordingly in SD.", runner.stageLurkers),
		SubcommandDefinition(kCommandNameSubmit, "Prepares the enlistment for changelist submission and submits the specified changelist.", runner.submit),
		SubcommandDefinition(kCommandNameSync, "Sync the enlistment to the most recent good change.", runner.sync),
		SubcommandDefinition(kCommandNameUnitTest, "Runs unit tests for this application", runner.unittest)
	]

	#Generate all of the subcommand parsers.
	for subcommandDefinition in allSubcommands:
		subcommandParsers[subcommandDefinition.commandName] = _initSubcommandParser(subparsers, subcommandDefinition)

	#The submit command also takes a CL number, so specify that too
	subcommandParsers[kCommandNameSubmit].add_argument("changelist", help="The changelist to be submitted.")

	return parser
