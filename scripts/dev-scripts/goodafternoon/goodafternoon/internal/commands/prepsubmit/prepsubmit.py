'''Defines functionality for
the "goodafternoon prepsubmit" command.
'''
from basis import Basis, CommandExecutionError, ScopedStep

from .internal import parseForBuildProjects, parseForTestProjects
from .. import Command
from ..build import Build

# String constants.
kTestCommandFormatString = "mbu test -m {0} -c debug"
kValidateChangesCommandString = ["mbu", "validatechanges"]
kStepName = "Prepare enlistment for submission"

class PrepSubmit(Command):
	'''Performs the PrepSubmit command.
	See ./readme.md.
	'''
	def __init__(self, basis: Basis):
		'''Instance initializer.

		:param basis: A non-None instance of Basis.

		:raises ParameterInvalidError: If basis is None.
		'''
		Command.__init__(self, basis)

	def run(self):
		'''The function used to call the PrepSubmit
		command from the subcommand parser.

		:return:	The return code received from
		the shell after running the
		PrepSubmit command.
		'''
		with ScopedStep(kStepName, self.basis) as _step:
			returnCode = 1
			try:
				# First, figure out required builds from
				# validatechanges
				validateChangesCommandOutput = self.basis.stepExecutor.shell.commandWithOutput(kValidateChangesCommandString, shell=False)

				# Parse the output for the projects that
				# must be built and the projects that
				# must be tested.
				projectsToBuild = parseForBuildProjects(validateChangesCommandOutput)
				numProjectsToBuild = len(projectsToBuild)

				projectsToTest = parseForTestProjects(validateChangesCommandOutput)
				numProjectsToTest = len(projectsToTest)

				# Build projects as needed
				if numProjectsToBuild:
					returnCode = Build(self.basis, " ".join(projectsToBuild)).run()

				# Early out if build failed
				if returnCode and numProjectsToBuild:
					return returnCode

				# Test projects as needed
				if numProjectsToTest:
					testCommandString = kTestCommandFormatString.format(
						" ".join(projectsToTest))
					self.basis.stepExecutor.shell.commandWithOutput(testCommandString)

				returnCode = 0
			except CommandExecutionError as e:
				returnCode = e.exit_code

			return returnCode
