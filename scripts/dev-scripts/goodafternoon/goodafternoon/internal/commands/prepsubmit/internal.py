'''Defines functions used internally in the prepsubmit module.
'''
import re

#Regex constants.
kTimestampHeaderRegex = r"\[\d\d\d\d/\d\d/\d\d\.\d\d:\d\d:\d\d\]"
#$1 will contain the projects requiring building/testing,
#separated by spaces.
kBasicRegex = "{0} Error: Failed or missing {{0}} {{1}} for projects:\s*\n\
{0} Error: (.*)\s*\n".format(kTimestampHeaderRegex)
kBuildDebugRegex = kBasicRegex.format("debug", "build")
kBuildShipRegex = kBasicRegex.format("ship", "build")
#Tests are always on the debug config, so no need to check the ship string
kTestRegex = kBasicRegex.format("debug", "test")

def _parseForRegex(terminalText: str, regex):
	'''Parses multiline text to find 
	the entry matching the given regex.

	:returns: A set of all strings found in the regex's
	capture group, split by spaces.
	'''
	#Parse the text for the given capture group.
	print(regex)
	match = re.search(regex, terminalText)

	#If the group's not empty, space-split it.
	result = set()
	if match and match.groups():
		result = set(match.group(1).split(" "))

	#Return the result as a set.
	return result

def parseForBuildProjects(terminalText: str):
	'''Parses multiline text to find 
	the entry listing all projects to be built.

	:returns: A set of strings that are the
	projects to be built.
	'''
	debugProjects = _parseForRegex(terminalText, kBuildDebugRegex)
	shipProjects = _parseForRegex(terminalText, kBuildShipRegex)

	#The ship projects may differ from the debug projects,
	#so merge the found projects together.
	return debugProjects.union(shipProjects)

def parseForTestProjects(terminalText: str):
	'''Parses multiline text to find 
	the entry listing all projects to be tested.

	:returns: A set of strings that are the
	projects to be tested.
	'''
	return _parseForRegex(terminalText, kTestRegex)
