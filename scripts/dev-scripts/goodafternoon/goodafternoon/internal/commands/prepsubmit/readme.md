# Summary
Prepares the project for submission, but does not actually submit any changelist.

## Steps
Specifically, this command does the following:
  1. Determine builds needed with validatechanges:
    1. ```mbu validatechanges``` (parse output for "needs building" section and need testing sections)
  2. Build the specified projects with commands.build.
  3. If and only if (2) succeeded, perform tests on the specified projects:
    1. ```mbu test -m [projects] -c debug```

This doesn't perform findlurkers, as it's unclear what should be done with lurkers if they're found.

## Return Code
The command returns the return code returned by this list of steps.