'''Defines functionality for
the "goodafternoon build" command.
'''
from basis import Basis, CommandExecutionError, ScopedStep

from .. import Command

kCommandFormatString = "((mbu build -m {0} -c debug ship) || (mbu build -md {0} -c debug ship)) || ((mbu nuke --branch || mbu nuke --branch) && mbu build -md {0} -c debug ship)"
kDefaultProject = "outlook"
kStepName = "Build project"

class Build(Command):
	'''Performs the Build command.
	See ./readme.md.
	'''
	def __init__(self, basis, projects=""):
		'''Instance initializer.

		:param basis: A non-None instance of Basis.
		:param str projects: The projects to be built.
		Generally this should just be "outlook",
		but for prepsubmit it could be multiple
		space-delimited names. If no projects are provided,
		defaults to "outlook".

		:raises ParameterInvalidError: If basis is None.
		'''
		Command.__init__(self, basis)
		self.projects = projects
		if not projects:
			self.projects = kDefaultProject

	def _doRun(self):
		'''The function that actually performs
		the Build command for the given list of projects.

		:param str projects:	The projects to be built.
								Generally this should just be
								"outlook", but for prepsubmit
								it could be multiple
								space-delimited names.

		:return:	The return code received from
					the shell after running the
					Build command.

		'''
		#See ./readme.md for derivation of
		#this command string.
		kCommandString = kCommandFormatString.format(self.projects)

		#Just run the command and exit
		returnCode = 1
		try:
			self.basis.stepExecutor.shell.command(kCommandString)
			returnCode = 0
		except CommandExecutionError as e:
			returnCode = e.exit_code

		return returnCode

	def run(self):
		'''The function used to call the Build
		command from the subcommand parser.

		:param basis: A non-None instance of Basis.

		:return:	The return code received from
					the shell after running the
					Build command.

		:raises ParameterInvalidError: If basis is None.
		'''
		with ScopedStep(kStepName, self.basis) as _step:
			return self._doRun()
