# Summary
This command performs a build by attempting the following:

## Steps
  1. Perform an incremental build:
    1. ```mbu build -m outlook -c debug ship```
  2. If and only if (1) failed, perform a dependency build:
    1. ```mbu build -md outlook -c debug ship```
  3. If and only if (1) and (2) failed, perform a nuke and dependency build:
    1. ```(mbu nuke --branch || mbu nuke --branch)```
    2. (3.1) ```&& mbu build -md outlook -c debug ship```

## Return Code
The command returns the return code returned by this list of steps.