# Summary
This command finds all lurkers in the project and stages them with zsh:

## Steps
  * ```findlurkers | zsh```

Since this command doesn't do anything else, it won't delete any lurking adds.

## Return Code
The command returns the return code of the single step performed.