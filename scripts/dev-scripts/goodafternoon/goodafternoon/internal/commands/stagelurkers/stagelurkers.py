'''Defines functionality for
the "goodafternoon stagelurkers" command.
'''
from basis import Basis, CommandExecutionError, ScopedStep

from .. import Command

kCommandString = "findlurkers | zsh"
kStepName = "Stage lurking changes"

class StageLurkers(Command):
	'''Performs the StageLurkers command.
	See ./readme.md.
	'''
	def __init__(self, basis: Basis):
		'''Instance initializer.

		:param basis: A non-None instance of Basis.

		:raises ParameterInvalidError: If basis is None.
		'''
		Command.__init__(self, basis)

	def run(self):
		'''The function used to call the StageLurkers
		command from the subcommand parser.

		:return: The return code received from
		the shell after running the
		StageLurkers command.
		'''
		with ScopedStep(kStepName, self.basis) as _step:
			#Just run the command and exit
			returnCode = 1
			try:
				self.basis.stepExecutor.shell.command(kCommandString)
				returnCode = 0
			except CommandExecutionError as e:
				returnCode = e.exit_code

			return returnCode
