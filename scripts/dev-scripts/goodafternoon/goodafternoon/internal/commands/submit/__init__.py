'''init module for goodafternoon.internal.test.commands.submit.
'''
from .submit import Submit, kValidateChangesCommandString, kSubmitChangesCommandFormatString
