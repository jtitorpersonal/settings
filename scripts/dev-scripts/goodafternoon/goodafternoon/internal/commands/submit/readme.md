# Summary
Prepares the project for submission, and **actually** submit the specified changelist.

#Invocation
goodafternoon submit [changelist number] (-f --force-lurkers)

-f, --force-lurkers: If set, any lurking changes are added to the default local changelist (0) via commands.stagelurkers.

#Execution Details
## Steps
Specifically, this command does the following:
  1. Run commands.prepsubmit.
  2. If and only if (1) succeeded:
    1. If ```-f``` was specified:
      1. Run commands.stagelurkers.
    2. If and only if (2.1) was successful or not true:
      1. ```mbu validatechanges```
      2. If and only if (2.2.1) was successful, run ```mbusd submit [changelist number]```.

## Return Code
The command returns the return code returned by this list of steps.