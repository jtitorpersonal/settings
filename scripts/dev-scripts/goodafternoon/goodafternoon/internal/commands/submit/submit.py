'''Defines functionality for
the "goodafternoon submit" command.
'''
import unittest

from basis import Basis, CommandExecutionError, ScopedStep

from ..prepsubmit import PrepSubmit
from ..stagelurkers import StageLurkers
from .. import Command

kValidateChangesCommandString = ["mbu", "validatechanges"]
kSubmitChangesCommandFormatString = ["mbusd", "submit"]

kStepName = "Submit changes"

class Submit(Command):
	'''Performs the Submit command.
	See ./readme.md.
	'''
	def __init__(self, basis: Basis, changeListIdString: str, forceStageLurkers=False):
		'''Instance initializer.

		:param basis.Basis basis: A non-None instance of Basis.
		:param str changeListIdString: The changelist's numeric ID as a string.

		:raises ParameterInvalidError: If basis is None.
		'''
		Command.__init__(self, basis)
		self.changeListIdString = changeListIdString
		self.forceStageLurkers = forceStageLurkers

	def run(self):
		'''The function used to call the submit
		command from the subcommand parser.

		:return: The return code received from
		the shell after running the
		Submit command.
		'''
		with ScopedStep(kStepName, self.basis) as _step:
			#First, prepare for submission
			returnCode = 1
			try:
				returnCode = PrepSubmit(self.basis).run()

				#Early out if prep failed
				if returnCode:
					return returnCode

				#If we're force staging lurkers, do that
				if self.forceStageLurkers:
					returnCode = StageLurkers(self.basis).run()
					#Again, early out if this failed
					if returnCode:
						return returnCode

				#Now do a single validatechanges...
				#kValidateChangesCommandString = "mbu validatechanges"
				self.basis.stepExecutor.shell.command(kValidateChangesCommandString, shell=False)

				#...and if that succeeded finally submit
				submitChangesCommandString = kSubmitChangesCommandFormatString + [self.changeListIdString,]
				self.basis.stepExecutor.shell.command(submitChangesCommandString, shell=False)

				returnCode = 0
			except CommandExecutionError as e:
				returnCode = e.exit_code

			return returnCode
