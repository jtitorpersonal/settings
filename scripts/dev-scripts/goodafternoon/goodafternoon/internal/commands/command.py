'''Defines the base class for commands.
'''
import unittest

from basis import Basis

from ..errors import ParameterInvalidError

class Command(object):
	'''Base class for runnable commands.
	'''

	def __init__(self, basis: Basis):
		'''Instance initializer.

		:param basis: A non-None instance of Basis.

		:raises ParameterInvalidError: If basis is None.
		'''
		if basis is None:
			raise ParameterInvalidError("Basis instance was None, can't initialize command")

		self.basis = basis

	def run(self):
		'''The function used to call this command from the command parser.

		:return:	The return code received from
					the shell after running the
					Sync command.
		'''
		#Default implementation does nothing beyond sanity check, so return success
		return 0
