'''Defines functionality for
the "goodafternoon sync" command.
'''
from basis import Basis, CommandExecutionError, ScopedStep

from .. import Command

kCommandString = "ocheck -fix; osync_apple sync; sd resolve"
kStepName = "Sync enlistment"

class Sync(Command):
	'''Performs the sync command.
	See ./readme.md.
	'''
	def __init__(self, basis: Basis):
		'''Instance initializer.

		:param basis: A non-None instance of Basis.

		:raises ParameterInvalidError: If basis is None.
		'''
		Command.__init__(self, basis)

	def run(self):
		'''The function used to call the Sync
		command from the subcommand parser.

		:return: The return code received from
		the shell after running the
		Sync command.
		'''
		with ScopedStep(kStepName, self.basis) as _step:
			#Just run the command and exit
			returnCode = 1
			try:
				self.basis.stepExecutor.shell.command(kCommandString)
				returnCode = 0
			except CommandExecutionError as e:
				returnCode = e.exit_code

			return returnCode
