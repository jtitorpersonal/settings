# Summary
Performs the sync command. This consists of the following steps:

##Steps
  1. ```ocheck -fix```
  2. ```osync_apple sync```
  3. ```sd resolve```

## Return Code
The command returns the return code performed by this list of steps.