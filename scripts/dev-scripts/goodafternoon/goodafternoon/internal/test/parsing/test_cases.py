'''Defines test cases for all parsing systems.
'''
import unittest

#from ...parsing import 

class TestCommands(unittest.TestCase):
	'''Contains unit tests for all subcommands.
	'''
	def __init__(self):
		'''Instance initializer.
		'''
		#Tests aren't defined yet; raise an exception here
		unittest.TestCase.__init__(self)
		raise NotImplementedError()
