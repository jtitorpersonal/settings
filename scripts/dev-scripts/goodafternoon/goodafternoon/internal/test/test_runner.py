'''Runs test commands.
'''
import unittest

from . import commands
from . import parsing

class TestRunner(object):
	'''Runs all unit tests in the application.
	'''

	def __init__(self):
		'''Instance initializer.
		'''
		self._loader = unittest.TestLoader()
		self._suite = unittest.TestSuite()

		# add tests to the test suite
		self._suite.addTests(parsing.testSuite())
		self._suite.addTests(commands.testSuite())

	def run(self):
		'''Runs all unit tests retrieved by this TestRunner.
		'''

		# initialize a runner, pass it your suite and run it
		runner = unittest.TextTestRunner(verbosity=3)
		result = runner.run(self._suite)

		return result
