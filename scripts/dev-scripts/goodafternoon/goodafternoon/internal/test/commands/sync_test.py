'''Defines functionality for
the "goodafternoon sync" command.
'''
import unittest

from ....internal.commands.sync import kCommandString

class SyncTest(object):
	'''Tests the sync command.
	See ./readme.md.
	'''
	@classmethod
	def test(cls, testCase: unittest.TestCase):
		'''Function used to perform unit testing on this command.

		:param unittest.TestCase testCase: the TestCase instance to run this test on.

		:raises Error: Other errors can be raised depending on how the test fails or breaks asserts.
		'''
		#ASSERT: command to run matches expected sync command
		kExpectedCommandString = "ocheck -fix; osync_apple sync; sd resolve"
		testCase.assertEqual(kCommandString, kExpectedCommandString)
