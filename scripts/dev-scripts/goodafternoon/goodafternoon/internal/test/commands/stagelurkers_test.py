'''Defines functionality for
the "goodafternoon stagelurkers" command.
'''
import unittest

from ....internal.commands.stagelurkers import kCommandString

class StageLurkersTest(object):
	'''Tests the StageLurkers command.
	See ./readme.md.
	'''
	@classmethod
	def test(cls, testCase: unittest.TestCase):
		'''Function used to perform unit testing on this command.

		:param unittest.TestCase testCase: the TestCase instance to run this test on.
		
		:raises Error: Other errors can be raised depending on how the test fails or breaks asserts.
		'''
		#ASSERT: command to run matches expected findlurkers command
		testCase.assertEqual(kCommandString, "findlurkers | zsh")
