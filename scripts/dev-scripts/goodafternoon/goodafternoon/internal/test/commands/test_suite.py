'''Runs test commands.
'''
import unittest

from . import test_cases

def testSuite():
	loader = unittest.TestLoader()
	suite = unittest.TestSuite()

	# add tests to the test suite
	suite.addTests(loader.loadTestsFromModule(test_cases))
	return suite
