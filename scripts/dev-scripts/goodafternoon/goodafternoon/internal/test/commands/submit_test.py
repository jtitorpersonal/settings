'''Defines functionality for
the "goodafternoon submit" command.
'''
import unittest

from ....internal.commands.submit import kValidateChangesCommandString, kSubmitChangesCommandFormatString

class SubmitTest(object):
	'''Tests the Submit command.
	See ./readme.md.
	'''
	@classmethod
	def test(cls, testCase: unittest.TestCase):
		'''Function used to perform unit testing on this command.

		:param unittest.TestCase testCase: the TestCase instance to run this test on.

		:raises Error: Other errors can be raised depending on how the test fails or breaks asserts.
		'''
		#ASSERT: command to validate changes matches expected validatechanges command
		testCase.assertEqual(kValidateChangesCommandString, ["mbu", "validatechanges"])

		#ASSERT: command to submit CL matches expected submit command with sample CL input
		kSampleClIdString = "1234567"
		submitChangesCommandString = kSubmitChangesCommandFormatString + [kSampleClIdString,]
		kExpectedSubmitChangesCommandString = ["mbusd", "submit", "1234567"]
		testCase.assertEqual(submitChangesCommandString, kExpectedSubmitChangesCommandString)
