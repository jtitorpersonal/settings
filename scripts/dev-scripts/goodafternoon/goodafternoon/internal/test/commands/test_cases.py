'''Defines test cases for all subcommands.
'''
import unittest

from .build_test import BuildTest
from .prepsubmit_test import PrepSubmitTest
from .stagelurkers_test import StageLurkersTest
from .submit_test import SubmitTest
from .sync_test import SyncTest

class TestCommands(unittest.TestCase):
	'''Contains unit tests for all subcommands.
	'''

	def testBuild(self):
		'''Runs unit tests on the Build command.
		'''
		BuildTest.test(self)

	def testPrepSubmit(self):
		'''Runs unit tests on the PrepSubmit command.
		'''
		PrepSubmitTest.test(self)

	def testStageLurkers(self):
		'''Runs unit tests on the StageLurkers command.
		'''
		StageLurkersTest.test(self)

	def testSubmit(self):
		'''Runs unit tests on the Submit command.
		'''
		SubmitTest.test(self)

	def testSync(self):
		'''Runs unit tests on the Sync command.
		'''
		SyncTest.test(self)
