'''Defines functionality for
the "goodafternoon prepsubmit" command.
'''
import unittest

from .parsing_sample_text import sampleTerminalOutput
from ....internal.commands.prepsubmit import parseForBuildProjects, parseForTestProjects
from ....internal.commands.prepsubmit import kTestCommandFormatString

# String constants.
kTestCommandFormatString = "mbu test -m {0} -c debug"

class PrepSubmitTest(object):
	'''Tests the PrepSubmit command.
	See ./readme.md.
	'''
	@classmethod
	def test(cls, testCase: unittest.TestCase):
		'''Function used to perform unit testing on this command.

		:param unittest.TestCase testCase: the TestCase instance to run this test on.

		:raises Error: Other errors can be raised depending on how the test fails or breaks asserts.
		'''
		# Parsing tests. Given sample validatechanges text:
		kSampleTerminalOutput = sampleTerminalOutput()

		# ASSERT: set of projects to build matches expected set
		projectsToBuild = parseForBuildProjects(kSampleTerminalOutput)
		kExpectedProjectsToBuild = set(
			[
				"target1",
				"target4",
				"target2",
				"target5",
				"target3"
			]
		)
		testCase.assertEqual(projectsToBuild, kExpectedProjectsToBuild)

		# ASSERT: set of projects to test matches expected set
		projectsToTest = parseForTestProjects(kSampleTerminalOutput)
		kExpectedProjectsToTest = set(
			[
				"target4",
				"target2",
				"target5",
				"target3"
			]
		)
		testCase.assertEqual(projectsToTest, kExpectedProjectsToTest)

		# ASSERT: test command string matches expected invocation
		#Alphabetically sort projects before
		#reconstructing string so assert passes
		projectsToTest = sorted(list(projectsToTest))
		testCommandString = kTestCommandFormatString.format(" ".join(projectsToTest))
		kExpectedTestCommandString = "mbu test -m target2 target3 target4 target5 -c debug"
		testCase.assertEqual(testCommandString, kExpectedTestCommandString)
