'''Defines functionality for
the "goodafternoon build" command.
'''
import unittest

from ....internal.commands.build import kCommandFormatString

class BuildTest(object):
	'''Tests the Build command.
	See ./readme.md.
	'''
	@classmethod
	def test(cls, testCase: unittest.TestCase):
		'''Function used to perform unit testing on this command.

		:param unittest.TestCase testCase: the TestCase instance to run this test on.

		:raises Error: Other errors can be raised depending on how the test fails or breaks asserts.
		'''
		#ASSERT: command to run matches expected build command with sample input
		kProjects = "test1 test2"
		kCommandString = kCommandFormatString.format(kProjects)
		kExpectedCommandString = "((mbu build -m test1 test2 -c debug ship) || (mbu build -md test1 test2 -c debug ship)) || ((mbu nuke --branch || mbu nuke --branch) && mbu build -md test1 test2 -c debug ship)"

		testCase.assertEqual(kCommandString, kExpectedCommandString)
