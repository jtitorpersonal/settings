'''Defines sample parsing text for unit tests.
'''
import os

def sampleTerminalOutput():
	'''Returns sample terminal text used for testing
	parsing.
	'''
	filePath = os.path.join(os.path.dirname(__file__), "parsing_sample_text.txt")

	with open(filePath, "r") as sampleFile:
		return sampleFile.read()
