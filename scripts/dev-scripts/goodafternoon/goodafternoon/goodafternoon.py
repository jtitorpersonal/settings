#!/usr/bin/python

#Add the current directory to the path so
#we can include bootstrap.
from os import sys, path

#TODO 11/15/2018: Unclear that this section is still needed
includePath = path.dirname(path.dirname(path.abspath(__file__)))
print(includePath) 
sys.path.append(includePath)

import argparse

from .internal import Runner

def main():
	Runner().parseArgsAndRun()

if __name__ == "__main__":
	main()
