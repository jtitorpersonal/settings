#!/usr/bin/python
#Syncs with main and performs resolve as needed.

import os
#Add the current directory to the path so
#we can include bootstrap.
from os import sys, path
includePath = path.dirname(path.dirname(path.abspath(__file__)))
print(includePath) 
sys.path.append(includePath)

from bootstrap import Bootstrap, StepExecutor, Logging, StepResult

class Steps(object):
	class RefreshKerberos(object):
		Name = "Refresh Kerberos ticket"

		@classmethod
		def check(cls, stepExecutor):
			return StepResult.NotDone

		@classmethod
		def install(cls, stepExecutor):
			return stepExecutor.shellCommand("Kerberos ticket refresh", 
				'kinit mileun@NORTHAMERICA.CORP.MICROSOFT.COM')

	class UpdateSources(object):
		Name = "Update sources"

		@classmethod
		def check(cls, stepExecutor):
			return StepResult.NotDone

		@classmethod
		def install(cls, stepExecutor):
			#Run one update.
			if not stepExecutor.shellCommand("Initial source update", "mbusd update"):
				#mbusd update doesn't report success
				#if any files had to be merged;
				#run it again in that case.
				return stepExecutor.shellCommand("Repeat source update", "mbusd update")
			return True

	class ResolveConflicts(object):
		Name = "Resolve conflicts"

		@classmethod
		def check(cls, stepExecutor):
			return StepResult.NotDone

		@classmethod
		def install(cls, stepExecutor):
			return stepExecutor.shellCommand("Source resolve", 
				'sd resolve')

def runSteps(step):
	if not step.perform(Steps.RefreshKerberos):
		Logging.error("Kerberos login failed, aborting!")
		return
	if not step.perform(Steps.UpdateSources):
		Logging.error("Update failed, aborting!")
		return
	step.perform(Steps.ResolveConflicts)

def main():
	bootstrap = Bootstrap()
	bootstrap.installStartMessage = "Starting morning sync..."
	bootstrap.checkStartMessage = "Checking sync status (this is kind of redundant)..."
	bootstrap.installFailedMessage = "Startup failed on following steps: "
	bootstrap.successMessage = "Morning startup complete!"

	errCode = bootstrap.run(runSteps)
	exit(errCode)

if __name__ == "__main__":
	main()