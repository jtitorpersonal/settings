from bootstrap import StepExecutor, Logging, StepResult

class Steps(object):
	class GetChangedProjects(object):
		Name = "Get changed projects"

		@classmethod
		def check(cls, stepExecutor):
			return StepResult.NotDone

		@classmethod
		def install(cls, stepExecutor):
			#Run mbu validatechanges.

			#Parse output:
			#	Errors are in the format "Warning: Failed or missing debug build for project: opf"
			#		Split these errors between build/test and debug/ship.
			return False

	class BuildProjects(object):
		Name = "Build changed projects"

		@classmethod
		def check(cls, stepExecutor):
			#Only skip if there's nothing in either build table.
			pass
			return StepResult.NotDone

		@classmethod
		def install(cls, stepExecutor):
			#Build all: "mbu build -d -m [build debug entries + build ship entries] -c debug ship".
			#	(if they're all debug or all ship, only have that mode after -c)
			return False

	class TestProjects(object):
		Name = "Test changed projects"

		@classmethod
		def check(cls, stepExecutor):
			#Only skip if there's nothing in either test table.
			pass
			return StepResult.NotDone

		@classmethod
		def install(cls, stepExecutor):
			#Test all: mbu test -m [test debug entries + test ship entries] -c debug ship".
			#	(if they're all debug or all ship, only have that mode after -c)
			return False

	class FindLurkers(object):
		Name = "Build changed projects"

		@classmethod
		def check(cls, stepExecutor):
			pass
			return StepResult.NotDone

		@classmethod
		def install(cls, stepExecutor):
			#Run mbu validatechanges again.
			#If any lurkers were found, list them here.
			#	This counts as a failure, too.
			#Otherwise report success.
			return False