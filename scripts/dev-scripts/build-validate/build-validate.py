#!/usr/bin/python
#Checks changed projects, and
#builds and tests them in preparation for a mbusd submit.
#Syntax is "build-validate.py".

#Add the current directory to the path so
#we can include bootstrap.
from os import sys, path
includePath = path.dirname(path.dirname(path.abspath(__file__)))
print(includePath) 
sys.path.append(includePath)

import re
from buildvalidate import Steps
from bootstrap import Bootstrap

def runSteps(step):
	step.perform(Steps.GetChangedProjects)
	#If build failed, abort now.
	if not step.perform(Steps.BuildProjects):
		return
	#If test failed, abort now.
	if not step.perform(Steps.TestProjects):
		return
	step.perform(Steps.FindLurkers)

def main():
	bootstrap = Bootstrap()
	bootstrap.installStartMessage = "Starting build/validate..."
	bootstrap.checkStartMessage = "Checking build/validate status (this is kind of redundant)..."
	bootstrap.installFailedMessage = "Build/validate failed on the following steps"
	bootstrap.successMessage = "Build/validate complete, ready for submit"

	errCode = bootstrap.run(runSteps)
	exit(errCode)

if __name__ == "__main__":
	main()