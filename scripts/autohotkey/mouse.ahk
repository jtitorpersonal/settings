;Alt+Shift+Q = left click
!+q::
	click
return

;Alt+Shift+E = right click
!+e::
	click right
return

;Alt+Shift+(WASD) = move mouse
;up, left, down, and right, respectively
!+w::
	mousemove, 0, -50, 2, R
return
!+a::
	mousemove, -50, 0, 2, R
return
!+s::
	mousemove, 0, 50, 2, R
return
!+d::
	mousemove, 50, 0, 2, R
return