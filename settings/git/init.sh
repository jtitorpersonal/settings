#!/usr/bin/env bash
WD="$(pwd)"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

configAlreadyExists=throw
#Does the user's .gitconfig exist?
if ! [ $configAlreadyExists ]; then
	#If not:
	#Copy the default and exit.
	cp default.gitconfig ~/.gitconfig
else
	#Otherwise:
	#Open default.gitconfig.
	sourceFile=throw
	#Get the default.gitconfig's
	#[alias]->EOF section.
	sourceAliasSection=throw

	#Get the file.
	destFile=throw
	#Find the [alias]->EOF section of the
	#.gitconfig and replace that section
	#with the default.gitconfig's
	#[alias]->EOF section.
	throw
fi

cd $WD