#Mostly trying to keep alias up to date.
$workingDir = $pwd.Path
$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition

$configAlreadyExists = throw
#Does the user's .gitconfig exist?
if(!$configAlreadyExists) {
	#If not:
	#Copy the default and exit.
	Copy-Item default.gitconfig ~/.gitconfig
}
else {
	#Otherwise:
	#Open default.gitconfig.
	$sourceFile = throw
	#Get the default.gitconfig's
	#[alias]->EOF section.
	$sourceAliasSection = throw

	#Get the file.
	$destFile = throw
	#Find the [alias]->EOF section of the
	#.gitconfig and replace that section
	#with the default.gitconfig's
	#[alias]->EOF section.
	throw
}

cd $workingDir