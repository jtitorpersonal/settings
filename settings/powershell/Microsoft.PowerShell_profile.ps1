#User profile script, used any time a PS shell is opened
#under your user.
#This includes things like the integrated shell in
#Visual Studio Code, although such programs are free
#to override their shell with additional settings.
#Create the initial blank profile with
#`New-Item -path $profile -type file –force`,
#then copy this file to the profile with
#`mv Microsoft.Powershell_profile.ps1 $profile -force`.

#Disable that fucking backspace bell!!!
Set-PSReadlineOption -BellStyle None