# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"

  #Disable the original box's folder definition since it breaks on Windows
  #(expects rsync for some reason)
  config.vm.synced_folder ".", "/vagrant", disabled: true

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  config.vm.provision "shell", inline: <<-SHELL
    #Make sure we actually have sudo...
    yum install -y sudo dos2unix

    #Setup folder for SSH.
    #Do *not* give root SSH credentials since a lot of provisioning tools
    #will set user-level data such as PATH. In that case we don't
    #want those tools succeeding when run as root.
    mkdir -p /home/vagrant/.ssh/
  SHELL

  #Now add SSH keys.
  config.vm.provision "file", source: "../ssh_config", destination: "/home/vagrant/.ssh/config"
  config.vm.provision "file", source: "../vm_key_rsa", destination: "/home/vagrant/.ssh/id_rsa"
  #Also copy Git aliases.
  config.vm.provision "file", source: "../../../settings/git/default.gitconfig", destination: "/home/vagrant/.gitconfig"
  config.vm.provision "file", source: "../provision.sh", destination: "/home/vagrant/provision.sh"
  config.vm.provision "file", source: "../gui-provision.sh", destination: "/home/vagrant/gui-provision.sh"

  #Do post-install provision.
  config.vm.provision "shell", privileged: false, inline: <<-SHELL
    cd /home/vagrant
    chmod 777 gui-provision.sh provision.sh
    #Make sure the scripts have proper line endings too.
    dos2unix gui-provision.sh provision.sh
    ./gui-provision.sh
  SHELL

  config.vm.provider "virtualbox" do |v|
    v.gui = true
  end
end
