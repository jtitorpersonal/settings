#!/bin/bash
set -x #echo on

#Post-up provision script.
#This is meant to be run as vagrant, not as root.

#Install the essentials:
# * Version control
# * Text editor
sudo yum install -y sudo git emacs
#It's not really necessary on CLI-only boxes,
#but alias emacs to start in text mode.
sudo -u vagrant -s "alias emacs=emacs -nw"

#Install less common tools.
#Do JDK instead of JRE since some things like Maven need the full JDK.
#As an aside - Linux really doesn't like us using /opt,
#since CentOS doesn't include /opt in PATH by default.
sudo yum install -y java-1.8.0-openjdk-devel
sudo yum -y update
sudo yum -y install epel-release

#Make sure SSH keys can be used by SSH.
chmod 0600 /home/vagrant/.ssh/id_rsa
chmod 0600 /home/vagrant/.ssh/config

echo "Attempting tool installation..."
git clone --recurse git@bitbucket.com:jtitorpersonal/tools.git /home/vagrant/tools
cd /home/vagrant
python -m tools.devbox.provision -v