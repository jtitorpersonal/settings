#!/usr/bin/env bash
WD="$(pwd)"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#Only installs the dev tools.
#Assumes the project repos have already been installed.

#We should be in position. Do the provisioning now.
cd ~/dev/basileus/dev && sudo python -m provision.run -v
if [ $? != 0 ] ; then
	echo "Dev tool provisioning failed!"
	return 1
fi

echo "Basileus dev setup complete!"
cd $WD