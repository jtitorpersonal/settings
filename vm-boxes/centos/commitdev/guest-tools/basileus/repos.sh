#!/usr/bin/env bash
WD="$(pwd)"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Gets the given repo and switches to branch wip
# Parameters:
#	* $1: Repo base name (e.g. "dev", not "git@.../dev.git).
getrepo() {
	git clone "git@bitbucket.org:jtitorpersonal/$1.git" && cd $1 && git checkout wip && git pull origin && git submodule init && git submodule update && cd ..
}

#Make sure SSH keys can be used by SSH.
chmod 0600 /home/vagrant/.ssh/config && chmod 0600 /home/vagrant/.ssh/id_rsa
if [ $? != 0 ] ; then
	echo "Couldn't configure SSH!"
	return 1
fi

echo "Pulling repositories..."
#Get all of the repositories.
cd ~/ && mkdir -p dev/basileus && cd dev/basileus && getrepo "dev" && getrepo "server" && getrepo "bas-build-server"
if [ $? != 0 ] ; then
	echo "Repository pull failed!"
	return 1
fi

echo "Repository pull complete."
cd $WD