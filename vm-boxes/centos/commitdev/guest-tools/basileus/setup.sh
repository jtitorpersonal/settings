#!/usr/bin/env bash
WD="$(pwd)"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

result=0
#Get the project repos.
source "$DIR/repos.sh"
if [ $? != 0 ] ; then
	echo "Warning! Couldn't pull project repos!"
	result=1
fi

#We should be in position. Do the provisioning now.
source "$DIR/tools.sh" 
if [ $? != 0 ] ; then
	echo "Dev tool provisioning failed!"
	result=1
fi

#Generate the vagrant key...
source "$DIR/../generatekey.sh"
if [ $? != 0 ] ; then
	echo "SSH key generation failed!"
	result=1
fi

if [ $result != 0 ] ; then
	echo "Basileus dev setup had errors! System may not be ready!"
	echo "You can retry with 'logout' followed by"
	echo "'./local-tools/basileus/up.ps1' or 'source local-tools/basileus/up.sh'."
else
	echo "Basileus dev setup complete!"
	echo "Display the public key with 'cat /home/vagrant/.ssh/id_rsa.pub'."
fi

cd $WD
return $result