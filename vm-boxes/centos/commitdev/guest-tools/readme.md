# Summary
Contains tools used to setup the guest for specific tasks after the general-purpose setup of `vagrant up`.

# Tool Summaries:
* `setupbasileus.sh`: Downloads and installs dev tools for Basileus (Go, Node.js/NPM, Python extensions such as zerorpc).