#!/usr/bin/env bash
WD="$(pwd)"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "Generating new SSH key for machine..."
#Delete any existing key.
rm -f ~/.ssh/id_rsa
cat /dev/zero | ssh-keygen -t rsa -q -N "" > /dev/null
if [ $? != 0 ] ; then
	echo "Key generation failed!"
	return 1
fi

echo "Key generated at '~/.ssh/id_rsa'!"
cd $WD