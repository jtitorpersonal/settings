# Summary
Generates a CentOS 7 dev box that's intended for *commiting* changes. Because of this, it emits its public key to stdout; use the key temporarily and revoke when you're done.

# How to Setup
Run `source local-tools/basileus/up.ps1` or `source local-tools/basileus/up.sh`, and copy the public key you're given into whatever repository system you use. The up script automatically opens a SSH session into the box, so you can start working on whatever you need.

If you're working on Basileus (editing the dev box, for instance), run `source ~/tools/basileus/tool.sh` within the guest after adding its public key to install the needed dev tools.

# How to Use
You can enter the system with `vagrant ssh` while in this folder, put it into sleep mode with `vagrant halt`, and resume it with `vagrant resume` (do *not* use `vagrant up` since this will completely reinitialize the VM).