#!/usr/bin/env bash
WD="$(pwd)"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#Attempts to destroy/up the nogui box,
#then provision the Basileus dev script (wip branch),
#then get the server code (wip branch).
echo "Provisioning box..."
cd "$DIR/../../" && vagrant destroy -f && vagrant up
if [ $? != 0 ] ; then
	echo "Box provision failed! Aborting!"
	return 1
fi
echo "Box provisioned! Pulling repositories..."

#Even if the repos don't pull right, generate the key.
vagrant ssh -c 'source ~/tools/basileus/repos.sh; source ~/tools/generatekey.sh'
if [ $? != 0 ] ; then
	echo "Repository pull and key gen failed! Aborting!"
	return 1
fi
echo "Box provisioned!"
echo "Call 'cat ~/.ssh/id_rsa.pub' to get the machine's public key."
echo "Starting SSH session..."
vagrant ssh
cd $WD