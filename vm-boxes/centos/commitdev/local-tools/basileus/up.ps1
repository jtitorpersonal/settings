#Attempts to destroy/up the nogui box,
#then provision the Basileus dev script (wip branch),
#then get the server code (wip branch).
$workingDir = $pwd.Path
$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition


#Pass input as a script block (surround with braces {}).
function invoke-failable() {
	invoke-command -ScriptBlock $args[0]
	if(!($?)) {
		cd $workingDir
		throw "Command '$args[0]' failed, aborting!"
	}
}
cd "$scriptPath/../.."
invoke-failable { vagrant destroy -f }
invoke-failable { vagrant up }
#Even if the repos don't pull right, generate the key.
invoke-failable { vagrant ssh -c 'source ~/tools/basileus/repos.sh; source ~/tools/generatekey.sh' }
invoke-failable { echo "Box provisioned!"
echo "Call 'cat ~/.ssh/id_rsa.pub' to get the machine's public key."
echo "Starting SSH session..." }
invoke-failable { vagrant ssh }
cd $workingDir