#Attempts to destroy/up the nogui box,
#then provision the Basileus dev script (wip branch),
#then get the server code (wip branch).

#Pass input as a script block (surround with braces {}).
function invoke-failable() {
	invoke-command -ScriptBlock $args[0]
	if(!($?)) {
		throw "Command '$args[0]' failed, aborting!"
	}
}
invoke-failable { vagrant destroy -f }
invoke-failable { vagrant up }
invoke-failable { echo "Box up, provisioning dev tools..." }
invoke-failable { vagrant ssh -c 'mkdir -p dev/basileus && cd dev/basileus && git clone git@bitbucket.org:jtitorpersonal/dev.git --recurse && git clone git@bitbucket.org:jtitorpersonal/server.git --recurse && cd dev && git checkout wip && git pull origin && git submodule init && git submodule update && sudo python -m provision.run -v && cd ../server && git checkout wip && git pull origin && git submodule init && git submodule update' }
invoke-failable { echo "Box provisioned! Entering ssh..." }
invoke-failable { vagrant ssh }