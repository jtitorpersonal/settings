#!/usr/bin/env bash

#Attempts to destroy/up the nogui box,
#then provision the Basileus dev script (wip branch),
#then get the server code (wip branch).
vagrant destroy -f && vagrant up && echo "Box up, provisioning dev tools..." && vagrant ssh -c 'mkdir -p dev/basileus && cd dev/basileus && git clone git@bitbucket.org:jtitorpersonal/dev.git --recurse && git clone git@bitbucket.org:jtitorpersonal/server.git --recurse && cd dev && git checkout wip && git pull origin && git submodule init && git submodule update && sudo python -m provision.run -v && cd ../server && git checkout wip && git pull origin && git submodule init && git submodule update' && echo "Box provisioned! Entering ssh..." && vagrant ssh