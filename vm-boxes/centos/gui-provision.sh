#!/bin/bash
set -x #echo on

#Run general provision first.
provisionPath="provision.sh"
if [ -e "$provisionPath" ]
then
	if source "$provisionPath"
	then
		echo "Common provision complete, starting GUI provision"
	else
		echo "Initial provision failed, aborting GUI provisioning!"
		exit 1
	fi
else
	echo "Can't find general provisioning script! Aborting!"
	exit 1
fi

#Also install a desktop manager...
sudo yum groupinstall -y "Server with GUI"
sudo yum groupinstall -y "Xfce"

#And ensure we go to graphics mode on startup.
sudo systemctl set-default graphical.target

#Finally reboot!
sudo reboot