# Summary
An automated Sierra testing box.

# How to Use
## Tester
To set up the tester box, fill out `../nonrepo/provision.credentials.txt` with your username on the first line and your password on the second line, then run `vagrant up` in this directory. Do not edit `user.credentials.txt`, it won't be copied to the box.