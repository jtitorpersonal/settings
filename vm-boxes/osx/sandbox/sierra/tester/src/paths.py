'''Stores all content paths
used by the project.
'''
import os

BaseDirectory = os.path.normpath(os.path.dirname(os.path.realpath(__file__)) + "/../content")

class Paths(object):
	'''Stores all content paths
	used by the project.
	'''

	class XCode(object):
		'''Stores XCode related paths.
		'''
		ArchivePath = BaseDirectory + "/xcode.xip"

		AllPaths = (ArchivePath,)
