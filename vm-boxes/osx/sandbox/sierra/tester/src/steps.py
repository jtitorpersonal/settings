'''Steps for provisioning the tester node.
'''

import urllib
import getpass
from os import path
from .basis import StepExecutor, StepResult, StepExecutorError, Logging
from .paths import Paths
try:
	#Also turn the messed up 2.x input()
	#into 3.x input().
	input = raw_input
except NameError:
	#If raw_input()'s missing, we're on 3.x,
	#so we're good.
	pass

sUser = ""
sPassword = ""

class Steps(object):
	'''Steps for provisioning the tester node.
	'''
	class GetCredentials(object):
		'''Gets credentials for connecting to necessary servers.
		'''
		Name = "Get credentials to connect to resource servers"
		_kUserPrompt = "Please enter your username: "
		_kPasswordPrompt = "Please enter your password: "

		@classmethod
		def check(cls, step):
			'''Checks if the given step
			has already been performed.
			'''
			return StepResult.Mandatory

		@classmethod
		def run(cls, step):
			'''Performs the given step.
			'''
			#Get username...
			#inUser = input(_kUserPrompt)
			#Get password.
			#inPassword = getpass.getpass(cls._kPasswordPrompt)
			#Get the username and password from a file.
			credentialFile = open(step.shell.normalizedPath(path.dirname(__file__) + "/../user.credentials.txt"))
			inUser = credentialFile.readline().rstrip("\n")
			inPassword = credentialFile.readline().rstrip("\n")

			global sUser
			global sPassword
			sUser = inUser
			Logging.verbose("Logging in as {0}".format(sUser), step.context)
			#Make sure the password is url encoded.
			sPassword = urllib.quote(inPassword)
			return StepResult.Success

	class XCodeIDEDownload(object):
		'''Downloads XCode.
		'''
		Name = "Download XCode IDE"

		@classmethod
		def check(cls, step):
			'''Checks if the given step
			has already been performed.
			'''
			assert isinstance(step, StepExecutor)

			#Check - is XCode installed?
			xcodeInstalled = step.shell.pathExists("/Applications/Xcode")
			#Can you get xcodebuild from the CLI?
			cliInstalled = step.shell.programExists("xcodebuild")
			if not (cliInstalled and xcodeInstalled):
				return StepResult.NotDone
			return StepResult.AlreadyDone

		@classmethod
		def run(cls, step):
			'''Performs the given step.
			'''
			assert isinstance(step, StepExecutor)
			#Make a temporary folder for any downloads.
			tempFolder = "~/Downloads/tester-provision"
			tempRepoMount = "~/mount/dev-tools"
			step.shell.command("mkdir -p {0}".format(tempFolder))
			step.shell.command("mkdir -p {0}".format(tempRepoMount))

			try:
				#Connect to the XCode repository.
				repoBaseName = "DevTools"
				fmtRemoteRepoPath = "//{0}officeapex/" + repoBaseName
				justRepoPath = fmtRemoteRepoPath.format("")
				loginRepoPath = fmtRemoteRepoPath.format("{0}:{1}@".format(sUser, sPassword))
				Logging.verbose("Connecting to {0} as {1}...".format(justRepoPath, sUser), step.context)
				step.shell.secureCommand("mount -t smbfs {0} {1}".format(loginRepoPath, tempRepoMount))
				#Get the most recent version.
				cmdLsLast = "ls | tail -1"
				cmdLsLastXip = "ls *.xip | tail -1"
				mostRecentMajorFolder = tempRepoMount + "/" + step.shell.commandWithOutput(cmdLsLast.format(tempRepoMount))
				mostRecentMinorFolder = mostRecentMajorFolder + "/" + step.shell.commandWithOutput(cmdLsLast.format(mostRecentMajorFolder))
				mostRecentXCodePath = mostRecentMinorFolder + "/" + step.shell.commandWithOutput(cmdLsLastXip.format(mostRecentMinorFolder))
				Logging.verbose("Remote path to XCode appears to be {0}".format(mostRecentXCodePath), step.context)

				#There should be a .xip; download it
				xipDownloadPath = tempFolder + "/most-recent-xcode.xip"
				step.shell.command("cp {0} {1}".format(mostRecentXCodePath, xipDownloadPath))
				#unpack it
				step.shell.unpackXip(xipDownloadPath)
				#and copy it to Applications.
				step.shell.command("mv {0} /Applications/".format(tempFolder + "Xcode"))
			except StepExecutorError as e:
				#Delete the temp folder.
				step.shell.command("rm -rf {0}".format(tempFolder))
				raise e

			#Also delete the xip once we're done
			step.shell.command("rm -rf {0}".format(tempFolder))
			#and disconnect from network resources.
			step.shell.command("umount {0}".format(tempRepoMount))
			step.shell.command("rm -rf {0}".format(tempRepoMount))
			return StepResult.Success

	class XCodeIDE(object):
		'''Installs XCode.
		'''
		Name = "Install XCode IDE"

		@classmethod
		def check(cls, step):
			'''Checks if the given step
			has already been performed.
			'''
			assert isinstance(step, StepExecutor)

			#Check - is XCode installed?
			xcodeInstalled = step.shell.pathExists("/Applications/Xcode")
			#Can you get xcodebuild from the CLI?
			cliInstalled = step.shell.programExists("xcodebuild")
			if not (cliInstalled and xcodeInstalled):
				return StepResult.NotDone
			return StepResult.AlreadyDone

		@classmethod
		def run(cls, step):
			'''Performs the given step.
			'''
			assert isinstance(step, StepExecutor)
			
			#Make sure the archive is actually
			#on the guest.
			xCodePath = step.shell.normalizedPath(Paths.XCode.ArchivePath)
			if not step.shell.pathExists(xCodePath):
				raise StepExecutorError("Couldn't find XCode archive '{0}', aborting!".format(xCodePath))

			#Make a temporary folder for any downloads.
			tempFolder = "~/Downloads/tester-provision"
			step.shell.command("mkdir -p {0}".format(tempFolder))

			try:
				#There should be a .xip; copy it
				#to a temp directory...
				xCodeTempPath = tempFolder + "/most-recent-xcode.xip"
				step.shell.command("cp {0} {1}".format(xCodePath, xCodeTempPath))
				#unpack it...
				step.shell.unpackXip(xCodeTempPath)
				#and copy it to Applications.
				step.shell.command("mv {0} /Applications/".format(tempFolder + "Xcode"))
			except StepExecutorError as e:
				#Delete the temp folder.
				step.shell.command("rm -rf {0}".format(tempFolder))
				raise e

			#Also delete the xip once we're done.
			step.shell.command("rm -rf {0}".format(tempFolder))
			return StepResult.Success

	class XCodeTools(object):
		'''Installs XCode command-line tools.
		'''
		Name = "Install XCode command-line tools"
		_xcodeInstalled = False
		#Special thanks to keen at
		#http://apple.stackexchange.com/questions/107307/how-can-i-install-the-command-line-tools-completely-from-the-command-line
		#for this command.
		_installToolsCommand = 'touch /tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress; PROD=$(softwareupdate -l | grep "\*.*Command Line" | head -n 1 | awk -F"*" \'{print $2}\' | sed -e \'s/^ *//\' | tr -d \'\n\'); softwareupdate -i "$PROD" -v;'

		@classmethod
		def check(cls, step):
			'''Checks if the given step
			has already been performed.
			'''
			assert isinstance(step, StepExecutor)

			#Check - is XCode installed?
			cls._xcodeInstalled = step.shell.pathExists("/Applications/Xcode")
			#Can you get xcodebuild from the CLI?
			cliInstalled = step.shell.programExists("xcodebuild")
			if not (cliInstalled and cls._xcodeInstalled):
				return StepResult.NotDone
			return StepResult.AlreadyDone

		@classmethod
		def run(cls, step):
			'''Performs the given step.
			'''
			assert isinstance(step, StepExecutor)

			#Abort if XCode isn't installed.
			if not cls._xcodeInstalled:
				raise StepExecutorError("XCode not installed, can't install CLI tools!")

			#Otherwise, let's do this!
			step.shell.command(cls._installToolsCommand)
			return StepResult.Success

	class ApexSetup(object):
		'''Installs Apex-related tools.
		'''
		Name = "Install Apex-related tools"

		@classmethod
		def check(cls, step):
			'''Checks if the given step
			has already been performed.
			'''
			assert isinstance(step, StepExecutor)

			raise NotImplementedError("ApexSetup.check() not implemented.")
			return StepResult.Success

		@classmethod
		def run(cls, step):
			'''Performs the given step.
			'''
			assert isinstance(step, StepExecutor)

			raise NotImplementedError("ApexSetup.run() not implemented.")
			return StepResult.Success
