#!/usr/bin/python
'''Provisions the tester node.
Installs ApexSetup and XCode.
'''

try:
	from .src.basis import Basis, StepExecutor, Logging
except ImportError as e:
	print("Couldn't import support libraries; did you init submodules?")
	print("(Init with 'git submodule init && git submodule update --remote --merge')")
	print("Error details: {0}".format(e))
	raise e
from .src.steps import Steps

def runSteps(step):
	'''Entry point for Basis.
	'''
	assert isinstance(step, StepExecutor)

	#Abort if this isn't macOS.
	if not step.context.osIsMacOS():
		raise RuntimeError("Provision script can only run on macOS systems!")

	#No need for this right now,
	#since XCode is now a copy from host.
	#step.perform(Steps.GetCredentials)

	#Install XCode first.
	step.perform(Steps.XCodeIDE)
	step.perform(Steps.XCodeTools)
	#Next, install ApexSetup if it's possible.
	try:
		step.perform(Steps.ApexSetup)
	except NotImplementedError:
		Logging.warning("Steps.ApexSetup not implemented, skipping")

def main():
	'''Entry point for the program.
	'''

	basis = Basis()
	basis.run(runSteps)

if __name__ == "__main__":
	main()
