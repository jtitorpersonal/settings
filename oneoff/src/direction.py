class Direction(object):
	def __init__(self, name, value):
		self.name = name
		self.value = value

class Directions(object):
	North = Direction("North", 0)
	South = Direction("South", 2)
	East = Direction("East", 1)
	West = Direction("West", 3)
	All = (North, East, South, West)

	@classmethod
	def opposite(cls, direction):
		'''Returns the opposite direction of the given direction object.
		'''
		return Directions.All[(direction.value + 2) % len(cls.All)]
