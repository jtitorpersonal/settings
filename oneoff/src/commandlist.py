from .actor import Actor
from .topology import Field

class CommandList(object):
	def __init__(self):
		#List of commands scheduled to be run.
		self.commands = []
		#Executed field-visible events go here,
		#indexed by field ID.
		#This is cleared at the beginning of each update.
		self.fieldEvents = {}

	def generateFieldChannels(self):
		#Prime the field event channel if possible.
		for field in Field.All:
			self.fieldEvents[field.fieldID] = []

	def addCommand(self, command):
		self.commands.append(command)

	def update(self, game):
		#While we still have commands...
		while self.commands:
			#Pop a command
			currCommand = self.commands.pop(0)
			#and execute it.
			if currCommand.execute(self):
				#If it ran, dispatch it to interested parties.
				#For now all events are public propagation,
				#so actor-specific channels aren't needed.
				self.fieldEvents[currCommand.originFieldID].append(currCommand)

	def render(self, game):
		#Clear all channels.
		for channelKey in self.fieldEvents:
			#Why doesn't Python 2 have clear()?
			del self.fieldEvents[channelKey][:]

class Commands(object):
	class Move(object):
		'''Moves the actor in the given direction by
		`length` number of units. If this would
		take it outside the field, the movement is
		clamped to the field's edge instead.
		'''
		def __init__(self, actorID, direction, length=1):
			self.actorID = actorID
			self.originFieldID = Actor.All[self.actorID].fieldID
			self.direction = direction
			self.length = length

		def render(self, viewerID):
			'''Performs a human-readable rendering of this event.
			'''
			actor = Actor.All[self.actorID]
			if self.actorID == viewerID:
				return "You move to {0}.".format(actor.fieldPosition)
			return "{0} moves to {1}.".format(actor.name, actor.fieldPosition)

		def execute(self, commandList):
			return Actor.All[self.actorID].move(self.direction, self.length)

	class MoveToField(object):
		'''Moves the actor from their current field
		to the destination field if possible. Does nothing if
		the actor can't actually reach the given field from their
		current field or the actor is already at the given field.
		'''
		def __init__(self, actorID, fieldID):
			self.actorID = actorID
			self.originFieldID = Actor.All[self.actorID].fieldID
			self.fieldID = fieldID

		def render(self, viewerID):
			'''Performs a human-readable rendering of this event.
			'''
			field = Field.All[self.fieldID]
			actor = Actor.All[self.actorID]
			if self.actorID == viewerID:
				return "You see yourself moving to field {0}???".format(field.fullName())
			return "{0} leaves the field.".format(actor.name)

		def execute(self, commandList):
			if Actor.All[self.actorID].moveToField(self.fieldID):
				#Also notify the field that the actor left.
				commandList.addCommand(Commands.MovedField(self.actorID))
				return True
			return False

	class MovedField(object):
		'''An indicator command that an actor has left
		the given field. Does nothing.
		'''
		def __init__(self, actorID):
			self.actorID = actorID
			self.originFieldID = Actor.All[self.actorID].fieldID

		def render(self, viewerID):
			'''Performs a human-readable rendering of this event.
			'''
			field = Field.All[self.originFieldID]
			actor = Actor.All[self.actorID]
			if self.actorID == viewerID:
				return "You enter field {0}".format(field.fullName())
			return "{0} enters the field.".format(actor.name)

		def execute(self, commandList):
			return True

	class EnterGate(object):
		'''Attempts to enter the field's gate,
		if it has one.
		'''
		def __init__(self, actorID):
			self.actorID = actorID
			self.originFieldID = Actor.All[self.actorID].fieldID

		def render(self, viewerID):
			'''Performs a human-readable rendering of this event.
			'''
			#Remember that this has been executed
			#and is thus happening on the *destination* field's
			#channel.
			actor = Actor.All[self.actorID]
			if self.actorID == viewerID:
				return "You see yourself entering the gate???."
			return "{0} enters the gate!.".format(actor.name)

		def execute(self, commandList):
			if Actor.All[self.actorID].enterGate():
				commandList.addCommand(Commands.EnteredGate(self.actorID))
				return True
			return False

	class EnteredGate(object):
		'''Observer version of EnterGate.
		'''
		def __init__(self, actorID):
			self.actorID = actorID
			self.originFieldID = Actor.All[self.actorID].fieldID

		def render(self, viewerID):
			'''Performs a human-readable rendering of this event.
			'''
			#Remember that this has been executed
			#and is thus happening on the *destination* field's
			#channel.
			actor = Actor.All[self.actorID]
			field = Field.All[self.originFieldID]
			if self.actorID == viewerID:
				return "You gate to {0}.".format(field.fullName)
			return "{0} exits the gate!.".format(actor.name)

		def execute(self, commandList):
			return True
