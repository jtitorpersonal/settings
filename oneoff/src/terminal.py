'''Contains terminal-specific operations.
'''
try:
	import curses
except ImportError:
	print "Platform doesn't appear to support Curses! Can't setup terminal!"
	import sys
	sys.exit(1)

class SnapCodes(object):
	'''Possible values for Window.snap.
	'''
	NoSnap = 0
	Up = 1
	Down = 2
	Left = 3
	Right = 4
	UpperLeft = 5
	UpperRight = 6
	LowerLeft = 7
	LowerRight = 8

class FillFlags(object):
	'''Possible flag values for Window.fill.
	'''
	NoFill = 0
	Up = 1 << 0
	Down = 1 << 1
	Left = 1 << 2
	Right = 1 << 3

class Palette(object):
	'''Represents a foreground/background pair.
	The colors come initialized with a Terminal;
	do not instantiate an instance as the colors won't
	actually work.
	'''
	def __init__(self, paletteCode, cursesForeground, cursesBackground):
		'''The palette's number in the palette.
		'''
		self.paletteCode = paletteCode
		'''The code this would have if curses.color_pair
		were called on its palette number.
		'''
		self.cursesCode = 0
		'''The curses constant that represents this
		palette's foreground color.
		'''
		self.cursesForeground = cursesForeground
		'''The curses constant that represents this
		palette's background color.
		'''
		self.cursesBackground = cursesBackground

class DefaultPalette(object):
	'''A set of common palette codes. An instance must be
	created by a Terminal first.
	'''
	def __init__(self):
		self.blackOnTransparent = Palette(1, curses.COLOR_BLACK, -1)
		self.whiteOnTransparent = Palette(2, curses.COLOR_WHITE, -1)
		self.redOnTransparent = Palette(3, curses.COLOR_RED, -1)
		self.greenOnTransparent = Palette(4, curses.COLOR_GREEN, -1)
		self.blueOnTransparent = Palette(5, curses.COLOR_BLUE, -1)
		self.whiteOnBlack = Palette(6, curses.COLOR_WHITE, curses.COLOR_BLACK)
		self.redOnBlack = Palette(7, curses.COLOR_RED, curses.COLOR_BLACK)
		self.greenOnBlack = Palette(8, curses.COLOR_GREEN, curses.COLOR_BLACK)
		self.blueOnBlack = Palette(9, curses.COLOR_BLUE, curses.COLOR_BLACK)
		self.whiteOnRed = Palette(10, curses.COLOR_WHITE, curses.COLOR_RED)
		self.whiteOnGreen = Palette(11, curses.COLOR_WHITE, curses.COLOR_GREEN)
		self.whiteOnBlue = Palette(12, curses.COLOR_WHITE, curses.COLOR_BLUE)
		'''All of the palettes as a tuple.
		'''
		self.all = (self.blackOnTransparent, self.whiteOnTransparent, self.redOnTransparent,
		self.greenOnTransparent, self.blueOnTransparent,
		self.whiteOnBlack, self.redOnBlack, self.greenOnBlack,
		self.blueOnBlack, self.whiteOnRed, self.whiteOnGreen,
		self.whiteOnBlue)

class FormatFlags(object):
	'''Formatting codes for character printing.
	'''

	'''Render the following text normally.
	'''
	Normal = 1 << 0
	'''Bold the text being displayed.
	For some colors this also intensifies
	the color.
	'''
	Bold = 1 << 1
	'''Use palette's foreground as background,
	and its background as foreground.
	'''
	Reverse = 1 << 2
	'''Underline the given character.
	'''
	Underline = 1 << 3
	'''Dim the given character.
	'''
	Dim = 1 << 4
	'''Make the character blink.
	'''
	Blink = 1 << 5
	'''Make the character as visible as possible.
	'''
	Standout = 1 << 6

	'''All formatting flags.
	'''
	All = (Normal, Bold, Reverse, Underline, Dim, Blink, Standout)
	'''Maps format flags to internal implementation values.
	'''
	ImplementationMap = {
		Normal: curses.A_NORMAL,
		Bold: curses.A_BOLD,
		Reverse: curses.A_REVERSE,
		Underline: curses.A_UNDERLINE,
		Dim: curses.A_DIM,
		Blink: curses.A_BLINK,
		Standout: curses.A_STANDOUT
	}

	@classmethod
	def toImplementationFlags(cls, flags):
		'''Converts a FormatFlags value to its
		implementation's respective value.
		'''
		result = 0
		for flag in cls.All:
			if flags & flag:
				result |= cls.ImplementationMap[flag]
		return result

class Window(object):
	'''Abstracts windows.
	'''
	LastID = -1
	FlagMap = {

	}

	@classmethod
	def _nextID(cls):
		'''Gets the next ID available for a window.
		'''
		cls.LastID += 1
		return cls.LastID

	def __init__(self, window, isStatic=False):
		'''The actual curses window object
		this window owns.
		'''
		self.window = window
		self.id = Window._nextID()
		#Set screen to no-delay
		#so key events don't block game execution.
		self.window.nodelay(True)
		'''If true, resize/change origin commands
		won't work and requestFill will be ignored.
		Generally only the main window needs this,
		but any window can have it set.
		'''
		self.isStatic = isStatic
		# '''If nonzero, this window will attempt to take the specified edges it can in the main window.
		# This is a *request*; if multiple windows have this set,
		# only one will get the fill and it's not guaranteed which
		# one is it.
		# '''
		# self.fillRequestFlags = fillRequestFlags
		# '''If nonzero, this window will attempt to snap to the specified edges in the main window.
		# Again, this is a request, and the first window in the terminal to request it gets to snap to the screen edge.
		# Other windows will snap to that window.
		# '''
		# self.snapRequest = snapRequest
		'''The (y,x) origin of the window.
		'''
		self.origin = self.window.getbegyx()
		'''The (y,x) lengths of the window sides.
		Is totally ignored if 'requestFill' is set.
		'''
		self.extents = self.window.getmaxyx()

	def clear(self):
		'''Clears the window area.
		'''
		self.window.clear()

	def shouldRefresh(self):
		'''Indicates that the window should be redrawn,
		but does not specifically do it; 'Terminal.refresh()'
		performs the actual redraw.
		'''
		self.window.noutrefresh()

	def refreshOrigin(self):
		'''If the origin was changed without us knowing,
		call this to update self.origin with the actual origin value.
		Returns:
			* True if the origin actually changed.
		'''
		newOrigin = self.window.getbegyx()
		originChanged = newOrigin != self.origin
		self.origin = newOrigin
		return originChanged

	def refreshExtents(self):
		'''The windowing system may have resized
		the terminal window; call this on the main window,
		otherwise 'self.extents' may no longer be valid.
		Returns:
			* True if the extents actually changed.
		'''
		newExtents = self.window.getmaxyx()
		extentsChanged = newExtents != self.extents
		self.extents = newExtents
		return extentsChanged

	def setOrigin(self, newOriginYX):
		'''Changes the window origin.
		Returns True if the window was moved
		to the exact position requested.
		'''
		if not self.isStatic:
			self.window.mvwin(newOriginYX[0], newOriginYX[1])
			self.refreshOrigin()
		return self.origin == newOriginYX

	def setExtents(self, newExtentsYX):
		'''Actually resizes the window.
		Returns True if the window was resized
		to the exact size requested.
		'''
		if not self.isStatic:
			self.window.resize(newExtentsYX[0], newExtentsYX[1])
			self.refreshExtents()
		return self.extents == newExtentsYX

	def getch(self):
		'''Gets a single character from
		standard input as an *integer*. Can be any positive value;
		you can trust that ASCII and Unicode values corespond as
		expected.
		Returns -1 when there's no input.

		To test for characters, do equality against ord(),
		or test equality against the curses constants for
		keys such as curses.KEY_LEFT.
		'''
		return self.window.getch()

	def setColor(self, backgroundPalette, formatFlags=FormatFlags.Normal):
		'''Sets the foreground/background to the given palette.
		'''
		assert isinstance(backgroundPalette, Palette)
		flags = FormatFlags.toImplementationFlags(formatFlags)
		self.window.bkgd(' ', backgroundPalette.cursesCode | flags)

	def _doPrintString(self, string, positionYX, cursesFlags=curses.A_NORMAL):
		self.window.addnstr(positionYX[0], positionYX[1], string, self.extents[1]-1, cursesFlags)

	def printString(self, string, positionYX=(0, 0)):
		'''Prints the given string, clipping it if
		it's too long for its window.
		'''
		self._doPrintString(string, positionYX)

	def printFormattedString(self, string, positionYX=(0, 0), flags=FormatFlags.Normal):
		'''Prints the given string with the
		given FormatFlags.
		'''
		self._doPrintString(string, positionYX, FormatFlags.toImplementationFlags(flags))

def _noLayout(_):
	'''Dummy function used when application
	doesn't provide a layout function.
	'''
	pass

class Terminal(object):
	'''Manages the terminal display and input.
	'''
	def __init__(self, window):
		#Set this or you end up with a totally black screen
		curses.use_default_colors()
		self.mainWindow = Window(window, isStatic=True)
		#We also don't really need to highlight the cursor.
		curses.curs_set(0)
		self.windows = [self.mainWindow,]
		'''Set this to ensure all windows are resized
		and refreshed.
		'''
		self.needsLayout = True
		#Initialize palettes.
		self.palette = DefaultPalette()
		for p in self.palette.all:
			curses.init_pair(p.paletteCode, p.cursesForeground, p.cursesBackground)
			p.cursesCode = curses.color_pair(p.paletteCode)

	def refresh(self, layoutFunction=_noLayout):
		'''Redraws the screen.
		'layoutFunction' is a callback you provide that
		takes the terminal as its only parameter;
		it should rearrange and resize the windows as you desire.
		if you don't provide one, a no-op dummy is used instead.
		'''
		#Check for size changes.
		screenResized = self.mainWindow.refreshExtents()
		#Don't actually lazy redraw
		#right now since you have to redraw
		#on screen change, and the screen's supposed to be dynamic.
		self.needsLayout = screenResized or self.needsLayout
		#if self.needsLayout:
		for window in self.windows:
			window.clear()
		layoutFunction(self)
		for window in self.windows:
			window.shouldRefresh()
		curses.doupdate()
		self.needsLayout = False

	def getch(self):
		'''Gets a single character from
		standard input. Can be any positive value;
		you can trust that ASCII values corespond as
		expected.
		Returns -1 when there's no input.

		To test for characters, do equality against ord(),
		or test equality against the curses constants for
		keys such as curses.KEY_LEFT.
		'''
		return self.mainWindow.getch()

	def _implementationSubWindow(self, originYX=(0, 0), extentsYX=(1, 1)):
		return curses.newwin(extentsYX[0], extentsYX[1], originYX[0], originYX[1])

	def subWindow(self, originYX=(0, 0), extentsYX=(1, 1), isStatic=False):
		'''Makes a subwindow at the given screen
		coordinates and extents, returning
		the Window object used to manipulate it.
		'''
		subwindow = self._implementationSubWindow(originYX, extentsYX)
		result = Window(subwindow, isStatic)
		self.windows.append(result)
		return result

	def removeWindow(self, windowID):
		'''Removes a window from rendering.
		The main window can't be removed this way,
		however.
		'''
		if windowID != self.mainWindow.id:
			windowToRemove = None
			for window in self.windows:
				if window.id == windowID:
					windowToRemove = window
			if windowToRemove:
				self.windows.remove(windowToRemove)

class manualTest(object):
	def __init__(self, stdscr):
		print("In init")
		#Make a terminal with stdscr.
		self.terminal = Terminal(stdscr)
		self.palette = self.terminal.palette
		#Display a world status section
		self.worldStatus = self.terminal.subWindow()
		#in red,
		self.worldStatus.setColor(self.palette.whiteOnRed)
		#An interpreter response section
		self.responseLine = self.terminal.subWindow()
		#in green,
		self.responseLine.setColor(self.palette.whiteOnGreen, FormatFlags.Bold)
		#and the interpreter command section
		self.commandLine = self.terminal.subWindow()
		self.commandLine.setColor(self.palette.whiteOnBlue, FormatFlags.Reverse)
		#in blue.
		print("Extents are: {0}").format(self.terminal.mainWindow.extents)

	def _manualTestLayout(self, _):
		#The command line should snap to the bottom left first
		#and fill right first. It should be 1 row high.
		screenExtents = tuple(self.terminal.mainWindow.extents)
		self.commandLine.setOrigin((screenExtents[0]-1, 0))
		self.commandLine.setExtents((1, screenExtents[1]))
		#The response line should snap to the bottom left second
		#and fill right second. It should be 1 row high.
		self.responseLine.setOrigin((screenExtents[0]-2, 0))
		self.responseLine.setExtents((1, screenExtents[1]))
		#The world status should snap to the upper left last
		#and fill bottom right last.
		self.worldStatus.setOrigin((0, 0))
		self.worldStatus.setExtents((screenExtents[0]-2, screenExtents[1]))
		#Optionally draw a border.
		# for window in self.terminal.windows[1:]:
		# 	window.window.border()
		#Add a message indicating the different windows.
		self.worldStatus.printString("The world status window! Should be white on red.")
		self.worldStatus.printString("(press 'Q' to end test)", (1, 0))
		self.responseLine.printString("The response line! Should be white on green and bold.")
		self.commandLine.printString("The command line! Should be blue on white.")

	def run(self):
		print("Entering run loop")
		#While we haven't hit Q:
		shouldQuit = False
		while not shouldQuit:
			#refresh the window.
			self.terminal.refresh(self._manualTestLayout)
			lastChar = self.terminal.getch()
			shouldQuit = lastChar == ord('q') or lastChar == ord('Q')

def _manualTest(stdscr):
	print("Running test")
	manualTest(stdscr).run()

def main():
	curses.wrapper(_manualTest)

if __name__ == "__main__":
	main()
