'''Specifies UI elements.
'''
from .terminal import Terminal, Window

class UI(object):
	'''UI elements.
	'''
	class UIElement(Window):
		'''Base class for UI elements.
		'''
		def __init__(self, terminal):
			#Make the window.
			implementationWindow = terminal._implementationSubWindow()
			#Attach ourselves to the terminal's window list.
			terminal.windows.append(self)
			#Initialize the super data now...
			super(UI.UIElement, self).__init__(implementationWindow)
			self.mainWindow = terminal.mainWindow

		def setRow(self, row):
			'''Convenience function for adjusting the row position
			only.
			'''
			self.setOrigin((row, self.origin[1]))

		def fillRow(self):
			'''Convenience function to fill entire row.
			'''
			self.setExtents((self.extents[0], self.mainWindow.extents[1]))

		def toBottom(self, displacement=0):
			'''Convenience function to snap to bottom minus
			'displacement'.
			'''
			self.setOrigin((self.mainWindow.extents[0]-(1+displacement), self.origin[1]))

		def toTop(self, displacement=0):
			'''Convenience function to snap to top plus
			'displacement'.
			'''
			self.setOrigin((displacement, self.origin[1]))

	class TextLine(UIElement):
		'''Displays a single string inside itself.
		This tries to have the same width as the screen.
		'''
		def __init__(self, terminal, label=""):
			super(UI.TextLine, self).__init__(terminal)
			#Now init our data.
			'''The text to display.
			'''
			self.label = label

		def render(self):
			'''Renders this TextLine.
			'''
			self.printString(self.label)

	class ScrollBox(UIElement):
		'''Displays multiple lines,
		removing the oldest as it goes.
		'''
		def __init__(self, terminal, bufferMax=80):
			super(UI.ScrollBox, self).__init__(terminal)
			#Now init our data.
			'''The lines currently being displayed.
			Always displays the *last* of these.
			'''
			self.lines = []
			'''The maximum number of lines
			stored before the oldest gets removed.
			'''
			assert bufferMax > 0
			self.bufferMax = bufferMax

		def addLine(self, stringToAdd=""):
			'''Adds a line to the box.
			If the line contains newlines,
			they're each considered a separate line.
			'''
			allStrings = stringToAdd.split('\n')
			for s in allStrings:
				self.lines.append(s)
			#Also pop off any excess from the top.
			while len(self.lines) > self.bufferMax:
				self.lines.pop(0)

		def render(self):
			'''Renders this ScrollBox.
			'''
			linesToRender = min(len(self.lines), self.extents[0])
			stringPosition = [0, 0]
			for s in self.lines[-linesToRender:]:
				self.printString(s, stringPosition)
				stringPosition[0] += 1

import random
import string
from .terminal import FormatFlags

class TestUI(object):
	'''Tests the UI elements.
	'''
	def __init__(self, window):
		self.terminal = Terminal(window)
		self.scrollBox = UI.ScrollBox(self.terminal)
		self.textLine1 = UI.TextLine(self.terminal, "Press 'A' to add a line...")
		self.textLine1.setColor(self.terminal.palette.whiteOnBlack)
		self.textLine2 = UI.TextLine(self.terminal)
		self.textLine2.label = "Or press 'Q' to quit"
		self.textLine2.setColor(self.terminal.palette.whiteOnBlack, FormatFlags.Reverse)

	def layout(self, _):
		self.textLine2.toBottom()
		self.textLine2.fillRow()
		self.textLine1.toBottom(1)
		self.textLine1.fillRow()
		screenExtents = self.terminal.mainWindow.extents
		self.scrollBox.setExtents((screenExtents[0]-2,screenExtents[1]))
		self.textLine2.render()
		self.textLine1.render()
		self.scrollBox.render()

	def addRandomLine(self):
		newLine = "".join([random.choice(string.letters) for i in xrange(random.randint(3, 90))])
		self.textLine2.label = "Added '{0}'".format(newLine)
		self.scrollBox.addLine(newLine)

	def run(self):
		#While we haven't hit Q:
		shouldQuit = False
		while not shouldQuit:
			#Refresh the window.
			lastChar = self.terminal.getch()
			if lastChar == ord('a') or lastChar == ord('A'):
				self.addRandomLine()
			shouldQuit = lastChar == ord('q') or lastChar == ord('Q')
			self.terminal.refresh(self.layout)

import curses
def testEntryPoint(stdscr):
	#Create and run an instance.
	TestUI(stdscr).run()

if __name__ == "__main__":
	curses.wrapper(testEntryPoint)