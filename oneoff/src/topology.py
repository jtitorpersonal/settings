from .direction import Direction, Directions
from .exceptions import WorldGenError

#World's composed of sectors, which have zones, which have fields.
#You can't really move around inside a field.

class Field(object):
	'''
	'''

	'''The table of all fields in use.
	Each field has an ID that is also its index into this array.
	All[0] is a null element that isn't connected to anything and can't be entered.
	'''
	All = []
	class Types(object):
		Generic = 0
		Gate = 1

	def __init__(self):
		self.name = "FIELD_NAME_UNSET"
		self.actors = []
		self.fieldID = len(Field.All)
		Field.All.append(self)
		self.owningZoneID = 0
		#Fields are square grids.
		self.gridSize = 10
		self.type = Field.Types.Generic

	def fullName(self):
		owningZone = Zone.All[self.owningZoneID]
		owningSector = Sector.All[owningZone.owningSectorID]
		return "{0} {1}".format(owningZone.fullName(), self.name)

	def coordinateToIndex(self, coordinateTuple):
		'''Converts the coordinates to an array index.
		'''
		return coordinateTuple.value[1] * self.gridSize + coordinateTuple.value[0]

	def coordinateIsValid(self, coordinateTuple):
		'''Returns True if the given coordinates are in the field's range,
		False otherwise.
		'''
		return coordinateTuple.value[0] < self.gridSize and coordinateTuple.value[1] < self.gridSize

class Gate(Field):
	'''A special type of field that lets you go to another zone.
	'''

	'''All gates. Note that Gate.All[0] is valid, since
	all gates are fields.
	'''
	All = []

	def __init__(self):
		super(Gate, self).__init__()
		self.destinationFieldID = 0
		Gate.All.append(self)
		self.type = Field.Types.Gate

	def linkTo(self, destinationGate):
		'''Makes a two-way link with another gate field.
		'''
		assert isinstance(destinationGate, Gate)
		#If we were already connected to another gate,
		#disconnect it from us first.
		if self.destinationFieldID != 0:
			Field.All[self.destinationFieldID].destinationFieldID = 0
		self.destinationFieldID = destinationGate.fieldID
		destinationGate.destinationFieldID = self.fieldID

class Zone(object):
	'''
	'''

	'''The table of all zones in use.
	Each zone has an ID that is also its index into this array.
	All[0] is a null element that isn't connected to anything and can't be entered.
	'''
	All = []

	'''One-way connections between zones.
	The index is the origin zone's ID, and the result is a list of zone IDs
	that the zone is connected to, indexed by north/south/east/west;
	You should use Zone.addLink to create links, but
	there's no reason you couldn't hand-generate links either.
	'''
	#_LinkLookup = 

	def __init__(self):
		self.name = "ZONE_NAME_UNSET"
		#The fields contained by this zone.
		self.fields = []
		self.zoneID = len(Zone.All)
		Zone.All.append(self)
		self.owningSectorID = 0
		#The N/S/E/W gates.
		self.gates = (Gate(), Gate(), Gate(), Gate())
		for gate in self.gates:
			self.addField(gate)

	def addField(self, field):
		'''Makes the given field a member of this zone.
		'''
		assert isinstance(field, Field)
		field.owningZoneID = self.zoneID
		#Fix up any reverse-lookup tables.
		self.fields.append(field)
		#raise NotImplementedError

	def listNeighbors(self):
		'''Lists all of the zones reachable from this zone.
		'''
		raise NotImplementedError

	def linkTo(self, destinationZone, direction):
		'''Adds a link between two zones, if it doesn't already exist. This zone's `direction` gate will connect to
		destinationZone's opposite direction gate.
		Raises:
			* WorldGenError if the origin zone already has a link in the given direction.
			* WorldGenError if twoWay is True and the destination zone has
			a link in the opposite direction.
		'''
		assert isinstance(destinationZone, Zone)
		assert isinstance(direction, Direction)

		#Connect the gates so the zones can reach each other.
		sourceGate = self.gates[direction.value]
		destinationGate = destinationZone.gates[Directions.opposite(direction).value]
		sourceGate.linkTo(destinationGate)

		#Fix up any neighbor tables.
		#raise NotImplementedError

	def fullName(self):
		return "{0} {1}".format(Sector.All[self.owningSectorID].fullName(), self.name)

class Sector(object):
	'''
	'''

	'''The table of all sectors in use.
	Each sector has an ID that is also its index into this array.
	All[0] is a null element that isn't connected to anything and can't be entered.
	'''
	All = []

	def __init__(self):
		self.name = "SECTOR_NAME_UNSET"
		#The zones contained by this sector.
		self.zones = []
		self.sectorID = len(Sector.All)
		Sector.All.append(self)
		#The inter-sector zones; these are the ways
		#in and out of the sector.
		#The zones correspond to the cardinal directions;
		#That zone's gate in that direction jumps to the
		#destination sector's opposite direction gate zone,
		#in the opposite direction gate.
		self.gateZones = (Zone(), Zone(), Zone(), Zone())
		for zone in self.gateZones:
			self.addZone(zone)

	def _doAddZone(self, zone):
		assert isinstance(zone, Zone)
		zone.owningSectorID = self.sectorID
		#Fix up any reverse-lookup tables.
		self.zones.append(zone)
		#raise NotImplementedError

	def addZone(self, zone):
		'''Makes the given zone a member of this sector.
		If `zone` is a tuple, all zones inside of the
		tuple are added.
		'''
		if hasattr(zone, '__iter__'):
			for z in zone:
				self._doAddZone(z)
		else:
			self._doAddZone(zone)

	def fullName(self):
		return self.name

	def linkTo(self, other, direction):
		'''Connects this sector to another zone's sector in the given direction.
		Raises WorldGenError if either sector has a connection that
		would be overriden by this connection.
		'''
		sourceGateZone = self.gateZones[direction.value]
		destinationGateZone = other.gateZones[Directions.opposite(direction).value]
		sourceGate = sourceGateZone.gates[direction.value]
		destinationGate = destinationGateZone.gates[Directions.opposite(direction).value]
		if sourceGate.destinationFieldID != 0:
			raise WorldGenError("Source gate already connected to another sector!")
		if destinationGate.destinationFieldID != 0:
			raise WorldGenError("Destination gate already connected to another sector!")
		sourceGate.linkTo(destinationGate)

class World(object):
	def __init__(self):
		self.sectors = []
		self.width = 0
		self.height = 0

	def addSector(self, sector):
		self.sectors.append(sector)
