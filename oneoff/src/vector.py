import math

class Vector(object):
	def __init__(self, x=0, y=0):
		self.value = [x, y]

	def __repr__(self):
		return "<{0}, {1}>".format(self.value[0], self.value[1])

	def __eq__(self, other):
		if isinstance(other, Vector):
			for i in range(0, len(self.value)):
				if self.value[i] != other.value[i]:
					return False
			return True
		else:
			return NotImplemented

	def __ne__(self, other):
		return not self.__eq__(other)

	def __neg__(self):
		result = Vector()
		for i in range(0, len(self.value)):
			result.value[i] = -self.value[i]
		return result

	def __add__(self, other):
		if isinstance(other, Vector):
			result = Vector()
			for i in range(0, len(self.value)):
				result.value[i] = self.value[i] + other.value[i]
			return result
		else:
			return NotImplemented

	def __sub__(self, other):
		return self.__add__(other.__neg__())

	def __mul__(self, other):
		if isinstance(other, int) or isinstance(other, float):
			result = Vector()
			for i in range(0, len(self.value)):
				result.value[i] = self.value[i] * other
			return result
		else:
			return NotImplemented

	def __div__(self, other):
		return self.__mul__(1.0 / other)

	def sqrMag(self):
		'''Returns the square magnitude of the vector.
		'''
		return self.value[0]**2 + self.value[1]**2

	def mag(self):
		'''Returns the magnitude of the vector.
		'''
		return math.sqrt(self.sqrMag())