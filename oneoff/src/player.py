from .topology import Field, Directions
from .actor import Actor
from .commandlist import Commands
from .ui import UI
from .debug_commands import DebugCommands

class Player(object):
	'''Displays world state and relays player input as world commands.

	There are two main sections: the world display
	and the command interpreter. The interpreter is split into
	two halves, a top line displaying system responses to their
	actions and a bottom line where the player enters commands.
	By default most commands are keyboard shortcuts, and '`' activates the interpreter mode.
	'''
	def _doControlActor(self, actorID):
		if actorID > len(Actor.All):
			raise RuntimeError("Invalid actor {0}".format(actorID))
		self.controlledActorID = actorID
		self.actor = Actor.All[self.controlledActorID]
		self._updateField()

	def _updateField(self):
		self.field = Field.All[self.actor.fieldID]

	def __init__(self, game, actorID=0):
		print("Entering world...")
		self._doControlActor(actorID)
		self.actionList = {
			ord('q'): game.quit,
			ord('w'): lambda: self.move(Directions.North),
			ord('s'): lambda: self.move(Directions.South),
			ord('a'): lambda: self.move(Directions.East),
			ord('d'): lambda: self.move(Directions.West),
			ord('f'): self.changeField,
			ord('g'): self.useGate,
			ord('l'): self.lookAll
		}
		self.commandList = game.commandList
		#UI elements.
		self.terminal = game.terminal
		self.uiWorldEvents = UI.ScrollBox(self.terminal)
		self.uiPlayerLocation = UI.TextLine(self.terminal)
		self.uiPlayerStatus = UI.TextLine(self.terminal)
		self.uiInterpreterResponse = UI.TextLine(self.terminal)
		self.uiInterpreterInput = UI.TextLine(self.terminal)
		self.uiPlayerLocation.setColor(self.terminal.palette.whiteOnBlack)
		self.uiPlayerStatus.setColor(self.terminal.palette.whiteOnBlack)
		self.uiInterpreterResponse.setColor(self.terminal.palette.whiteOnBlack)
		self.uiInterpreterInput.label = ">"
		self.uiInterpreterResponse.label = "What next?"
		self.uiAll = (self.uiWorldEvents, self.uiPlayerLocation, self.uiPlayerStatus, self.uiInterpreterResponse, self.uiInterpreterInput)

	def updateUI(self):
		self.uiInterpreterResponse.label = "What next?"
		self.uiPlayerLocation.label = self.actorLocation()
		self.uiPlayerStatus.label = self.actorState()
		for fieldEvent in self._getFieldChannel():
			self.uiWorldEvents.addLine(fieldEvent.render(self.controlledActorID))
		for uiElement in self.uiAll:
			uiElement.render()

	def layout(self, _):
		'''UI layout callback.
		'''
		self.uiInterpreterInput.toBottom()
		self.uiInterpreterResponse.toBottom(1)
		self.uiPlayerStatus.toBottom(2)
		self.uiWorldEvents.toTop(1)
		self.uiPlayerLocation.toTop()
		self.uiInterpreterInput.fillRow()
		self.uiInterpreterResponse.fillRow()
		self.uiPlayerStatus.fillRow()
		screenExtents = self.terminal.mainWindow.extents
		self.uiWorldEvents.setExtents((screenExtents[0]-4, screenExtents[1]))
		self.uiPlayerLocation.fillRow()
		self.updateUI()

	def controlActor(self, actorID):
		'''Immediately takes control of an actor.
		'''
		self._doControlActor(actorID)
		self.uiInterpreterResponse.label = "You are now {0}.".format(self.actor.name)

	def respawn(self):
		'''Creates a new actor and puts you in control
		of them, destroying the old actor you might've been in.
		'''
		raise NotImplementedError

	def describeSelf(self):
		return "You are {0}.".format(self.actor.name)

	def describeField(self):
		fieldDescription = "You are in {0}.".format(self.field.fullName())
		if self.field.fieldID == 0:
			fieldDescription += "\nYou get the feeling you shouldn't be here..."
		if self.field.type == Field.Types.Gate:
			otherField = Field.All[self.field.destinationFieldID]
			fieldDescription += "\nThis is a gate field! It's pointing to {0}.".format(otherField.fullName())
		return fieldDescription

	def lookAll(self):
		'''Looks at everything in the field.
		'''
		#Print to world status even though this isn't a event.
		self.uiWorldEvents.addLine(self.describeField())

	def actorState(self):
		return("Position:{0}, Armor: {1}/{2}, Psyche: {3}/{4}".format(self.actor.fieldPosition, self.actor.armor, self.actor.maxArmor, self.actor.psyche, self.actor.maxPsyche))

	def actorLocation(self):
		return "{0}: {1}".format(self.actor.name, self.field.fullName())

	def describeActor(self, otherActorID):
		if otherActorID == self.controlledActorID:
			self.describeSelf()
			return

		actor = Actor.All[otherActorID]
		print("You see a {0}.".format(actor.name))

	def move(self, direction):
		'''Attempts to move the player's actor in
		the given direction.
		'''
		self.commandList.addCommand(Commands.Move(self.controlledActorID, direction))
		self.uiInterpreterResponse.label = "Moving {0}...".format(direction.name)

	def changeField(self):
		'''Prompts for a field to go to,
		then tries to go to that field.
		'''
		pass
		self._updateField()

	def useGate(self):
		'''Uses the field's gate, if it has one.
		'''
		if not self.field.type == Field.Types.Gate:
			self.uiInterpreterResponse.label = "This isn't a gate field."
		else:
			self.commandList.addCommand(Commands.EnterGate(self.controlledActorID))
			self._updateField()

	def debugLookAll(self):
		playerInfo = DebugCommands.showActorDetails(self.controlledActorID)
		fieldInfo = DebugCommands.showFieldDetails(self.actor.fieldID)
		self.uiWorldEvents.addLine("{0}\n{1}".format(playerInfo, fieldInfo))

	def update(self, game):
		#Get player input here and send commands here.
		playerInput = game.terminal.getch()
		if playerInput in self.actionList:
			#If input's a valid command, run it.
			self.actionList[playerInput]()

	def _getFieldChannel(self):
		return self.commandList.fieldEvents[self.actor.fieldID]

	def render(self):
		#Display game world here.
		self.terminal.refresh(self.layout)
