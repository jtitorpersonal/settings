'''Defines actors!
'''
from .topology import Field
from .direction import Direction
from .vector import Vector
import random

class Actor(object):
	'''Any distinct entity in the game world.
	'''

	'''All actors. All[0] is an invalid element.
	'''
	All = []

	def __init__(self):
		self.name = "NO NAME SET"
		self.actorID = len(Actor.All)
		Actor.All.append(self)
		self.maxHealth = 1
		self.maxArmor = 1
		self.maxPsyche = 1
		self.health = self.maxHealth
		self.armor = self.maxArmor
		self.psyche = self.maxPsyche
		self.fieldID = 0
		self.fieldPosition = Vector(0, 0)

	def _doFieldChange(self, newFieldID):
		'''Performs the actual field transition.
		'''
		oldField = Field.All[self.fieldID]
		newField = Field.All[newFieldID]
		oldField.actors.remove(self)
		self.fieldID = newFieldID
		newField.actors.append(self)

	def moveToField(self, fieldID):
		'''Moves the actor from their current field
		to the destination field if possible. Does nothing if
		the actor can't actually reach the given field from their
		current field or the actor is already at the given field.
		'''
		#You can only go to another field in the same zone.
		if Field.All[fieldID].owningZoneID != Field.All[self.fieldID].owningZoneID or fieldID == self.fieldID:
			return False
			#warning ("Trying to go to an invalid field!")
		#Otherwise, set the field.
		self._doFieldChange(fieldID)
		return True

	def enterGate(self):
		'''Attempts to enter the field's gate,
		if it has one.
		'''
		field = Field.All[self.fieldID]
		if field.type != Field.Types.Gate:
			return False
		self._doFieldChange(field.destinationFieldID)
		return True

	def move(self, direction, length=1):
		'''Moves the actor in the given direction by
		`length` number of units. If this would
		take it outside the field, the movement is
		clamped to the field's edge instead.
		'''
		assert isinstance(direction, Direction)
		field = Field.All[self.fieldID]
		assert isinstance(field, Field)
		assert field.gridSize > 1, "Field #{0} has invalid grid size {1}".format(field.fieldID, field.gridSize)
		vertical = 1 if direction.value % 2 == 0 else 0
		horizontal = 1 if not vertical else 0
		magnitude = 1 if (direction.value / 2) < 1 else -1
		finalMag = length * magnitude
		moveVector = Vector(horizontal, vertical) * finalMag
		newPosition = self.fieldPosition + moveVector
		assert moveVector.sqrMag() > 0, "Vector is direction {0} and length {1} but returned a magnitude of {2}".format(direction, length, moveVector.sqrMag())
		#Clamp the position.
		for i in range(0, 2):
			newPosition.value[i] = max(0, min(newPosition.value[i], field.gridSize-1))
		self.fieldPosition = newPosition
		return True

	@classmethod
	def _doRandomSpawn(cls):
		'''Randomly places a new actor.
		'''
		actor = Actor()
		actor.fieldID = random.randint(1, len(Field.All)-1)
		actor.name = "Nemo {0}".format(actor.fieldID)
		return actor.actorID

	@classmethod
	def randomSpawnForView(cls, view):
		'''Creates a new actor, randomly places it,
		and puts it under control of the given view.
		'''
		actorID = cls._doRandomSpawn()
		view.controlActor(actorID)

	@classmethod
	def destroyActor(cls, actorID):
		'''Removes the given actor
		from the world.
		'''
		#No point actually removing a row in the actor table;
		#instead send the actor to the deadlands.
		#Since they probably don't have a view controlling them
		#they won't get updated,
		#and we can scan the table for inactive actors if we
		#need to add more.
		Actor.All[actorID].fieldID = 0

	@classmethod
	def respawnView(cls, view):
		oldActorID = view.controlledActorID
		cls.randomSpawnForView(view)
		cls.destroyActor(oldActorID)
