'''Defines exceptions used by the game.
'''

class GameException(RuntimeError):
	'''Base class for all game exceptions.
	'''
	pass

class GameplayError(GameException):
	'''Raised when an error occurs during gameplay.
	'''
	pass

class WorldGenError(GameException):
	'''Errors during world generation.
	'''
	pass

# Warning exceptions.
# these are unusual situations that
# won't crash the program, but probably will
# break the intended workflow.
# Unless we're developing you probably want to halt
# when these are generated,
# so they throw #unless --permit-warnings is set.
class GameplayWarning(GameException):
	'''Raised when a nonfatal error occurs during gameplay.
	'''
	pass
