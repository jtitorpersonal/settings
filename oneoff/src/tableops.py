'''Functions for initializing game tables.
'''
from .topology import Field, Zone, Sector
from .actor import Actor

class TableOps(object):
	class Nulls(object):
		'''Contains null objects.
		'''
		Field = Field()
		Zone = Zone()
		Sector = Sector()
		Actor = Actor()

		@classmethod
		def setup(cls):
			cls.Field.name = "0xBAADF00D"
			cls.Zone.name = "0xDEAD"
			cls.Sector.name = "0xFF"
			cls.Actor.name = "580087734"
			assert cls.Field.fieldID == 0
			assert cls.Zone.zoneID == 0
			assert cls.Sector.sectorID == 0
			assert cls.Actor.actorID == 0

	@classmethod
	def setup(cls):
		cls.Nulls.setup()
