from time import sleep
from timeit import default_timer as timer
from .worldgen import WorldGen
from .tableops import TableOps
from .commandlist import CommandList
from .actor import Actor
from .player import Player

class Game(object):
	def __init__(self, terminal):
		self.shouldQuit = False
		self.world = None
		#All the players in the game, basically.
		#update() is called on these elements
		#to update the world state.
		self.views = []
		self.commandList = CommandList()
		self.wallTimeSeconds = 0.0
		self.minUpdatableTimeSeconds = 0.01
		self.sleepTime = 0.01
		self.terminal = terminal
		#Prime the data tables!
		TableOps.setup()

	def runWithNewWorld(self):
		print("Generating world...")
		self.world = WorldGen.generateWorld()
		#Add one view for the player.
		self.views.append(Player(self))
		for view in self.views:
			Actor.randomSpawnForView(view)
		self.gameLoop()

	def update(self):
		'''Updates the game world. This also calls all views'
		update() method.
		'''
		for view in self.views:
			view.update(self)
		#Actually execute those view requests.
		self.commandList.update(self)

	def render(self):
		#Render all views.
		for view in self.views:
			view.render()
		self.commandList.render(self)

	def gameLoop(self):
		#Prep the command list.
		self.commandList.generateFieldChannels()
		#Connect the player and AIs to their actors.
		currTimeSeconds = timer()
		accumulator = 0.0
		while not self.shouldQuit:
			newTimeSeconds = timer()
			elapsedTimeSeconds = newTimeSeconds - currTimeSeconds
			accumulator += elapsedTimeSeconds

			#Enter the main game loop:
			#	Get player commands.
			#	Get AI commands.
			#	Validate commands.
			#	Update game world given all valid commands.
			while accumulator >= self.minUpdatableTimeSeconds:
				self.update()
				accumulator -= self.minUpdatableTimeSeconds
				self.wallTimeSeconds += self.minUpdatableTimeSeconds

			#	Display state to player.
			#	Display state to AI.
			self.render()
			sleep(self.sleepTime)

	def quit(self):
		self.shouldQuit = True
