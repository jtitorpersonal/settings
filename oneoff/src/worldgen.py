'''Code for world generation.
'''
from .topology import Field, Zone, Sector, World
from .direction import Directions
from .exceptions import WorldGenError
import random

def randomNameWithUsedList(nameList, usedIndexList):
	namesLen = len(nameList)
	nameIndex = random.randint(0, namesLen-1)
	while nameIndex in usedIndexList:
		nameIndex = random.randint(0, namesLen-1)
	usedIndexList.append(nameIndex)
	return(nameList[nameIndex])

class WorldGen(object):
	'''
	'''
	#Generic names when one isn't provided.
	FieldNames = ("Aria", "Bellows", "Camelia", "Delight",
	"Element", "Forest", "Garnet", "Hall",
	"Inquest", "Judge", "Kaleidoscope", "Lyric",
	"Mezzanine", "Nemesis", "Omen", "Park",
	"Relic", "Shell", "Temple", "Undine",
	"Veil", "Watchtower", "Xanadu", "Yarrow",
	"Zero")
	ZoneNames = ("Absolute", "Barred", "Carved", "Driven",
	"Exquisite", "Fated", "Glittering", "Hospitable",
	"Infinite", "Jealous", "King's", "Languid",
	"Merciful", "Obligated", "Parted", "Queen's",
	"Revived", "Stolen", "Triumphant", "Uniting",
	"Victorious", "Welcoming", "Xenotic", "Youthful",
	"Zealous")
	SectorNames = ("Alpha", "Beta", "Gamma", "Delta",
	"Epsilon", "Zeta", "Eta", "Theta",
	"Iota", "Kappa", "Lambda", "Mu",
	"Nu", "Xi", "Omicron", "Pi",
	"Rho", "Sigma", "Tau", "Upsilon",
	"Phi", "Chi", "Psi", "Omega")

	@classmethod
	def _makeSectorColumn(cls, columnLength):
		result = [Sector()]
		for i in range(1, columnLength):
			result.append(Sector())
			result[i-1].linkTo(result[i], Directions.North)
		return result

	@classmethod
	def _linkSectorColumn(cls, column1, column2):
		'''Links column1's west gates to column2's
		east gates.
		'''
		for i in range(0, len(column1)):
			currentSector1 = column1[i]
			currentSector2 = column2[i]
			assert isinstance(currentSector1, Sector)
			assert isinstance(currentSector2, Sector)
			currentSector1.linkTo(currentSector2, Directions.West)

	@classmethod
	def _generateSectorsForWorld(cls, world):
		if world.width * world.height > len(WorldGen.SectorNames):
			raise WorldGenError("Trying to generate more sectors than have unique names!")

		sectorColumns = []
		sectorColumns.append(cls._makeSectorColumn(world.height))
		for i in range(1, world.width):
			sectorColumns.append(cls._makeSectorColumn(world.height))
			cls._linkSectorColumn(sectorColumns[i], sectorColumns[i-1])
		#Now complete the loop.
		cls._linkSectorColumn(sectorColumns[0], sectorColumns[world.width-1])
		for col in sectorColumns:
			for sector in col:
				world.addSector(sector)

		#Do naming and other characterization here.
		usedNameIndices = []
		for sector in world.sectors:
			sector.name = randomNameWithUsedList(WorldGen.SectorNames, usedNameIndices)

	@classmethod
	def _generateZonesForSector(cls, sector, size):
		#Size is the n'th odd number.
		zoneRadius = 2*size-1
		#Max out at size 3 since there's not enough unique names
		#at that point.
		if size > 2:
			raise WorldGenError("Trying to generate more zones than have unique names!")
		if size < 0:
			raise WorldGenError("Size is out of bounds!")

		sectorZones = []
		if size == 1:
			#Just place the one zone and link it
			#to all of the gates.
			assert isinstance(sector, Sector)
			zone = Zone()
			for direction in Directions.All:
				zone.linkTo(sector.gateZones[direction.value], direction)
			sectorZones.append(zone)
		else:
			columns = []
			#Create the zone columns...
			for i in range(0, zoneRadius):
				columnStartZone = Zone()
				sectorZones.append(columnStartZone)
				column = [columnStartZone]
				columnLength = max((2*i+1, zoneRadius))
				for j in range(1, columnLength):
					zone = Zone()
					column.append(zone)
					sectorZones.append(zone)
					column[j-1].linkTo(column[j], Directions.North)
				columns.append(column)
			#Link the columns together...
			for i in range(1, len(columns)):
				currColumn = columns[i]
				prevColumn = columns[i-1]
				minLength = min(len(prevColumn), len(currColumn))
				currMid = len(currColumn) / 2
				prevMid = len(prevColumn) / 2
				for j in range(0, minLength):
					currColumn[currMid-(minLength/2)+j].linkTo(prevColumn[prevMid-(minLength/2)+j], Directions.West)

		for zone in sectorZones:
			sector.addZone(zone)

		#Actually describe the sectors here.
		usedNameIndices = []
		for zone in sector.zones:
			zone.name = randomNameWithUsedList(WorldGen.ZoneNames, usedNameIndices)
			assert zone.name

	@classmethod
	def _generateFieldsForSector(cls, sector, maxNumNewFields):
		#Add size asserts for the name limit for now.
		namesLen = len(WorldGen.FieldNames)
		if maxNumNewFields+4 > namesLen:
			raise WorldGenError("Trying to generate more fields than have unique names!")

		for zone in sector.zones:
			assert isinstance(zone, Zone)
			#Add new fields here.
			numNewFields = random.randint(3, maxNumNewFields)
			for i in range(0, numNewFields):
				newField = Field()
				#Vary the grid size.
				newField.gridSize = random.randint(10, 30)
				zone.addField(newField)
			usedNameIndices = []
			for field in zone.fields:
				#Give a name.
				field.name = randomNameWithUsedList(WorldGen.FieldNames, usedNameIndices)
				assert field.name

	@classmethod
	def generateWorld(cls):
		'''Returns a new randomly-generated world.
		'''
		world = World()
		world.width = 8
		world.height = 3
		#Generate the sectors.
		cls._generateSectorsForWorld(world)
		#Now do the zones...
		for s in world.sectors:
			cls._generateZonesForSector(s, random.randint(1, 2))
			#Now do the fields.
			cls._generateFieldsForSector(s, 20)

		return world
