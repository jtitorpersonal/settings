'''Displays debug info.
'''
from .actor import Actor
from .topology import Field

class DebugCommands(object):
	@classmethod
	def showActorDetails(cls, actorId):
		'''Displays all information about an actor.
		'''
		if len(Actor.All) <= actorId:
			return "Actor ID {0} is out of range, can't display info".format(actorId)

		actor = Actor.All[actorId]

		result = "Displaying info for actor #{0}:".format(actorId)
		assert isinstance(actor, Actor)
		result += "\n\tName: {0}".format(actor.name)
		result += "\n\tHealth: {0}/{1}".format(actor.health, actor.maxHealth)
		result += "\n\tArmor: {0}/{1}".format(actor.armor, actor.maxArmor)
		result += "\n\tPsyche: {0}/{1}".format(actor.psyche, actor.maxPsyche)
		return result

	@classmethod
	def showFieldDetails(cls, fieldId):
		'''Displays all information about a field.
		'''
		if len(Field.All) <= fieldId:
			return "Field ID {0} is out of range, can't display info".format(fieldId)

		field = Field.All[fieldId]
		result = "Displaying info for field #{0}:".format(fieldId)
		assert isinstance(field, Field)
		result += "\n\tName: {0}".format(field.name)
		result += "\n\tSize: {0}".format(field.gridSize)
		result += "\n\tType: {0}".format(field.type)
		result += "\n\tOwning Zone ID: {0}".format(field.owningZoneID)
		return result
