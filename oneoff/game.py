try:
	import curses
except ImportError:
	print "Platform doesn't appear to support Curses! Can't setup terminal!"
	import sys
	sys.exit(1)
from .src.game import Game
from .src.terminal import Terminal

def startGame(stdscr):
	game = Game(Terminal(stdscr))
	#There's no save/load,
	#so for now we just generate a new world each time.
	game.runWithNewWorld()

def main():
	curses.wrapper(startGame)

if __name__ == "__main__":
	main()
