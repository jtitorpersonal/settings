Runs batch operations on Git repositories. Current operations:
  * `clone`: Cloning repositories and remotes from a JSON specification
  * `validate`: Checks that a JSON specification can be used by `gitup clone`
  * `pull`: Updating repositories within a directory tree
  * `export`: Exporting repositories and their remotes to a JSON specification
  * `getbranches`: Listing currently active branch of repositories within a directory tree