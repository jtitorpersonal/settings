# Gittools
Helper app for managing a directory tree of Git repositiories.

You mainly want to refer to the Setuptools page: https://pythonhosted.org/an_example_pypi_project/setuptools.html

# Testing Changes
You can run the app as a normal package by running ```python -m gittools.gittools```, then passing normal flags. To unit test you are totally boned because there *are* no unit tests; if you want to implement those have fun with that.

# Building the App
Run ```python setup.py sdist```; this makes the uploadable package. To build the wheel file, use ```python setup.py bdist_wheel```. If that fails, make sure you have the wheel package with ```pip install wheel```.

# Publishing Changes
You publish changes by calling ```twine upload dist/*``` after the sdist call (again, make sure you have twine with ```pip install twine```); don't use ```setup upload```, as it may send your credentials in plaintext. You should have a ~/.pypirc set up with your PyPI username and password; if not the upload will fail. You also need to make sure the version in setup.py is newer than whatever's on the server.